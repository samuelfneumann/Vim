" =============================================================================
" Filename: autoload/lightline/colorscheme/catppuccin.vim
" Author: Samuel Neumann
" Last Change: 2022/12/12 06:16:00
" =============================================================================
let s:p = {'terminal': {}, 'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

" Set colours
let s:p.normal.left    = [ [  8, 4, 'bold' ], [ 4, 8, 'bold,italic'], [ 9, 8, 'bold' ], [ 13, 8, 'bold' ]]
let s:p.normal.middle  = [ [ 15, 8 ] ]
let s:p.normal.right   = [ [ 8, 4, 'bold' ], [ 8, 4, 'bold' ], [ 4, 8, 'bold,italic' ], [15, 8 ], [ 15, 8 ], [1, 8, 'bold' ], [ 3, 8, 'bold' ], [ 2, 8, 'bold' ], [5, 8, 'bold'] ]
let s:p.normal.error   = [ [ 8, 1, 'bold,italic' ] ]
let s:p.normal.warning = [ [ 8, 11, 'bold,italic' ] ]

let s:p.inactive.left   = [ [ 8, 7, "bold" ], [ 7, 8, "bold,italic"], [ 2, 8, 'bold' ], [ 13, 8, 'bold' ]]
let s:p.inactive.middle = [ [ 8, 8 ] ]
let s:p.inactive.right  = [ [ 8, 7, "bold" ], [ 8, 7, "bold" ], [ 7, 8, 'bold,italic' ], [ 7, 8 ], [ 7, 8 ], [1, 8, 'bold' ], [ 3, 8, 'bold' ], [ 2, 8, 'bold' ], [5, 8, 'bold'] ]

let s:p.insert.left     = [ [ 8, 2, 'bold' ], [ 2, 8, "bold,italic" ], [2, 8, 'bold' ], [ 13, 8, 'bold' ]]
let s:p.insert.right   = [ [ 8, 2, 'bold' ], [ 8, 2, 'bold' ], [ 2, 8, 'bold,italic' ], [15, 8 ], [ 15, 8 ], [1, 8, 'bold' ], [ 3, 8, 'bold' ], [ 2, 8, 'bold' ], [5, 8, 'bold'] ]

let s:p.replace.left    = [ [ 8, 1, 'bold' ], [ 1, 8, "bold,italic" ], [ 2, 8, 'bold' ], [ 13, 8, 'bold' ]]
let s:p.replace.right  = [ [ 8, 1, 'bold' ], [ 8, 1, 'bold' ], [ 1, 8, 'bold,italic'], [15, 8 ], [ 15, 8 ], [1, 8, 'bold' ], [ 3, 8, 'bold' ], [ 2, 8, 'bold' ], [5, 8, 'bold'] ]

let s:p.visual.left     = [ [ 8, 3, 'bold' ], [ 3, 8, "bold,italic" ], [ 2, 8, 'bold' ], [ 13, 8, 'bold' ]]
let s:p.visual.right   = [ [ 8, 3, 'bold' ], [ 8, 3, 'bold' ], [ 3, 8, 'bold,italic'], [15, 8 ], [ 15, 8 ], [1, 8, 'bold' ], [ 3, 8, 'bold' ], [ 2, 8, 'bold' ], [5, 8, 'bold'] ]

let s:p.terminal.left   = [ [ 8, 10, 'bold' ], [ 10, 8, "bold,italic" ], [ 2, 8, 'bold' ], [ 13, 8, 'bold' ]]
let s:p.terminal.right = [ [ 8, 10, 'bold' ], [ 8, 10, 'bold' ], [ 10, 8, 'bold,italic' ], [15, 8 ], [ 15, 8 ], [1, 8, 'bold' ], [ 3, 8, 'bold' ], [ 2, 8, 'bold' ], [5, 8, 'bold'] ]

let s:p.tabline.left   = [ [ 15, 8 ] ]
let s:p.tabline.tabsel = [ [ 8, 11, 'bold,italic' ] ]
let s:p.tabline.middle = [ [ 15, 8 ] ]
let s:p.tabline.right  = [ [ 8, 1, 'bold' ] ]

let g:lightline#colorscheme#catppuccin_mocha#palette = lightline#colorscheme#fill(s:p)
