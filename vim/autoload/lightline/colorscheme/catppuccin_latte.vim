" =============================================================================
" Filename: autoload/lightline/colorscheme/catppuccin_latte.vim
" Author: Samuel Neumann
" Last Change: 2022/12/12 06:16:00
" =============================================================================
let s:p = {'terminal': {}, 'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

" Set colours
let s:p.normal.left     = [ [  0, 4, 'bold' ], [ 0, 7 ] ]
let s:p.normal.middle   = [ [ 0, 7 ] ]
let s:p.normal.right   = [ [ 0, 4, 'bold' ], [ 0, 4, 'bold' ] ]
let s:p.normal.error   = [ [ 1, 11 ] ]
let s:p.normal.warning = [ [ 3, 15 ] ]

let s:p.inactive.left   = [ [ 7, 7 ], [ 7, 7 ] ]
let s:p.inactive.middle = [ [ 7, 7 ] ]
let s:p.inactive.right  = [ [ 7, 7 ], [ 7, 7 ] ]

let s:p.insert.left     = [ [ 0, 2, 'bold' ], [ 0, 7 ] ]
let s:p.insert.right   = [ [ 0, 2, 'bold' ], [ 0, 2, 'bold' ] ]

let s:p.replace.left    = [ [ 0, 1, 'bold' ], [ 0, 7] ]
let s:p.replace.right  = [ [ 0, 1, 'bold' ], [ 0, 1, 'bold' ] ]

let s:p.visual.left     = [ [ 0, 3, 'bold' ], [ 0, 7] ]
let s:p.visual.right   = [ [ 0, 3, 'bold' ], [ 0, 3, 'bold' ] ]

let s:p.terminal.left   = [ [ 0, 10, 'bold' ], [ 0, 7 ] ]
let s:p.terminal.right = [ [ 0, 10, 'bold' ], [ 0, 10, 'bold' ] ]

let s:p.tabline.left   = [ [ 0, 7 ] ]
let s:p.tabline.tabsel = [ [ 15, 11, 'bold,italic' ] ]
let s:p.tabline.middle = [ [ 0, 7 ] ]
let s:p.tabline.right  = [ [ 7, 1, 'bold' ] ]

let g:lightline#colorscheme#catppuccin_latte#palette = lightline#colorscheme#fill(s:p)
