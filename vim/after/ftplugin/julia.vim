" Textwidth ----------------------------------------------------------------{{{
" Julia text width is 92 characters
autocmd FileType julia setlocal textwidth=92
"}}}

" Format options -----------------------------------------------------------{{{
set formatoptions+=t " Auto wrap text using textwidth
set formatoptions+=c " Auto wrap comments using textwidth
set formatoptions+=r " Automatically insert comment leader on <ENTER>
set formatoptions+=n " Recognize numbered lists when formatting
set formatoptions-=l " Long lines should be broken in insert mode
set formatoptions+=j " Remove comment leader when joining lines
set formatoptions+=q " Allow formatting of comments with "gq"
"}}}

syntax match vimSlimeCell `### .* ###`


syntax keyword juliaRepKeyword continue conceal cchar=↻
syntax keyword juliaRepKeyword break conceal cchar=↪
syntax keyword juliaKeyword return conceal cchar=↵
" syntax keyword juliaBlKeyword function conceal cchar=𝑓

syntax match juliaTypeOperator "<:"
