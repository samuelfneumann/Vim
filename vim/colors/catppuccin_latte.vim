set background=dark
highlight clear
syntax reset

if !&termguicolors
	set t_Co=16
endif

let g:colors_name="catppuccin_latte"
let colors_name="catppuccin_latte"

let  s:colour0    =  {  "gui":  "#eff1f5",  "cterm":  "0"   }  "  Base
let  s:colour1    =  {  "gui":  "#d20f39",  "cterm":  "1"   }  "  Red
let  s:colour2    =  {  "gui":  "#40a02b",  "cterm":  "2"   }  "  Green
let  s:colour3    =  {  "gui":  "#df8e1d",  "cterm":  "3"   }  "  Yellow
let  s:colour4    =  {  "gui":  "#1e66f5",  "cterm":  "4"   }  "  Blue
let  s:colour5    =  {  "gui":  "#8839fe",  "cterm":  "5"   }  "  Mauve
let  s:colour6    =  {  "gui":  "#04a5e5",  "cterm":  "6"   }  "  Sky
let  s:colour7    =  {  "gui":  "#ccd0da",  "cterm":  "7"   }  "  Surface    1
let  s:colour8    =  {  "gui":  "#8c8fa1",  "cterm":  "8"   }  "  Overlay    1
let  s:colour9    =  {  "gui":  "#ea76cb",  "cterm":  "9"   }  "  Pink
let  s:colour10   =  {  "gui":  "#fe640b",  "cterm":  "10"  }  "  Peach
let  s:colour11   =  {  "gui":  "#dc8a78",  "cterm":  "11"  }  "  Rosewater
let  s:colour12   =  {  "gui":  "#7287fd",  "cterm":  "12"  }  "  Lavender
let  s:colour13   =  {  "gui":  "#179299",  "cterm":  "13"  }  "  Teal
let  s:colour14   =  {  "gui":  "#e64553",  "cterm":  "14"  }  "  Maroon
let  s:colours15  =  {  "gui":  "#4c4f69",  "cterm":  "15"  }  "  Text

let s:fg		  = { "gui": "#4c4f69", "cterm": "15" }
let s:bg          = { "gui": "#eff1f5", "cterm": "none" }

let s:comment_fg  = { "gui": "#acb0be", "cterm": "8", "attr": "italic" }
let s:linenr_bg   = s:bg
let s:linenr_fg   = { "gui": "#acb0be", "cterm": "8", "attr": "bold" }
let s:non_text    = s:colour13
let s:cursor_line = { "gui": "#8c8fa1", "cterm": "7", "attr": "" }
let s:color_col   = s:colour8
let s:status_line = s:colour8
let s:status_line_nc = s:colour7
let s:selection   = s:colour7
let s:vertsplit   = s:colour7
let s:gutter_bg   = s:bg

let s:pmenu_fg    = s:fg
let s:pmenu_bg    = s:colour7
let s:pmenu_selection   = s:colour11

" Highlight trailing colours15space
exec "hi _TrailingWhitespace" . " guibg=" . s:colour12.gui . " ctermbg=" .  s:colour12.cterm
match _TrailingWhitespace /\s\+$/
augroup TrailingWhitespace
	autocmd!
	autocmd BufWinEnter * match _TrailingWhitespace /\s\+$/
	autocmd InsertEnter * match _TrailingWhitespace /\s\+\%#\@<!$/
	autocmd InsertLeave * match _TrailingWhitespace /\s\+$/
	autocmd BufWinLeave * call clearmatches()
augroup end

" Set the terminal colours if termguicolors is set or in a GUI
if has('gui_running') || has('termguicolors') && &termguicolors
	let g:terminal_ansi_colors = [
				\ s:colour0.gui,
				\ s:colour1.gui,
				\ s:colour2.gui,
				\ s:colour3.gui,
				\ s:colour4.gui,
				\ s:colour5.gui,
				\ s:colour6.gui,
				\ s:colour7.gui,
				\ s:colour8.gui,
				\ s:colour9.gui,
				\ s:colour10.gui,
				\ s:colour11.gui,
				\ s:colour14.gui,
				\ s:colour13.gui,
				\ s:colour12.gui,
				\ s:colours15.gui,
				\]
endif

" Function to auto-highlight
function! s:h(group, fg, bg, attr)
	exec "hi clear " . a:group
  if type(a:fg) == type({})
    exec "hi " . a:group . " guifg=" . a:fg.gui . " ctermfg=" . a:fg.cterm
  else
    exec "hi " . a:group . " guifg=NONE cterm=NONE"
  endif
  if type(a:bg) == type({})
    exec "hi " . a:group . " guibg=" . a:bg.gui . " ctermbg=" . a:bg.cterm
  else
    exec "hi " . a:group . " guibg=NONE ctermbg=NONE"
  endif
  if a:attr != ""
    exec "hi " . a:group . " gui=" . a:attr . " cterm=" . a:attr
  else
    exec "hi " . a:group . " gui=NONE cterm=NONE"
  endif
endfun

" Theme settings
" User interface colors {
" call s:h("Normal", s:fg, s:bg, "")
call s:h("Normal", s:fg, "", "")

call s:h("Cursor", s:bg, s:colour4, "")
call s:h("CursorColumn", "", s:cursor_line, "")
call s:h("CursorLine", "", s:cursor_line, s:cursor_line.attr)

" call s:h("LineNr", s:linenr_fg, s:linenr_bg, "")
call s:h("LineNr", s:linenr_fg, "", "")
call s:h("CursorLineNr", s:fg, "", s:linenr_fg.attr)

call s:h("DiffAdd", s:colour2, "", "")
call s:h("DiffChange", s:colour3, "", "")
call s:h("DiffDelete", s:colour1, "", "")
call s:h("DiffText", s:colour4, "", "")

call s:h("IncSearch", s:colour0, s:colour14, "")
call s:h("Search", s:colour0, s:colour3, "")

call s:h("netrwMarkFile", s:colour0, s:colour1, "")
call s:h("netrwExe", s:colour2, "", "")
call s:h("netrwCompress", s:colour1, "", "")
call s:h("netrwMakeFile", s:colour11, "", "")
call s:h("netrwPix", s:colour3, "", "")
call s:h("netrwDoc", s:colour13, "", "")
call s:h("netrwTilde", s:colour11, "", "")

call s:h("ErrorMsg", s:fg, "", "")
call s:h("ModeMsg", s:fg, "", "")
call s:h("MoreMsg", s:fg, "", "")
call s:h("WarningMsg", s:colour1, "", "")
call s:h("Question", s:colour5, "", "")

call s:h("Pmenu", s:pmenu_fg, s:pmenu_bg, "")
call s:h("PmenuSel", s:colour0, s:pmenu_selection, "bold")
call s:h("PmenuSbar", "", s:pmenu_bg, "")
call s:h("PmenuThumb", "", s:pmenu_selection, "")

call s:h("SpellBad", "", "", "underline")
call s:h("SpellCap", s:colour4, "", "")
call s:h("SpellLocal", s:colour13, "", "")
call s:h("SpellRare", s:colour11, "", "")

call s:h("StatusLine", s:fg, s:status_line, "")
call s:h("StatusLineNC", s:fg, s:status_line_nc, "")
call s:h("TabLine", s:fg, s:status_line, "")
call s:h("TabLineFill", s:fg, s:status_line, "")
call s:h("TabLineSel", s:fg, s:colour11, "bold")

if has('gui_running') || (has('termguicolors') && &termguicolors)
	call s:h("Visual", "", s:selection, "")
	call s:h("VisualNOS", "", s:selection, "")
else
	call s:h("Visual", "", s:selection, "")
	call s:h("VisualNOS", "", s:selection, "")
endif

call s:h("ColorColumn", s:colour0, s:color_col, "")
call s:h("Conceal", s:fg, "", "")
call s:h("Directory", s:colour4, "", "")
call s:h("VertSplit", s:vertsplit, s:vertsplit, "")
call s:h("Folded", s:fg, "", "")
call s:h("FoldColumn", s:fg, "", "")
call s:h("SignColumn", s:fg, "", "")

call s:h("MatchParen", "", "", "bold,underline")
call s:h("SpecialKey", s:fg, "", "")
call s:h("Title", s:colour2, "", "")
call s:h("WildMenu", s:fg, "", "")

call s:h("Folded", s:colour13, "", "italic")
" }


" Syntax colors {
" colours15space is defined in Neovim, not Vim.
" See :help hl-colours15space and :help hl-SpecialKey
call s:h("colours15space", s:non_text, "", "")
call s:h("NonText", s:non_text, "", "")
call s:h("Comment", s:comment_fg, "", s:comment_fg.attr)
call s:h("Constant", s:colour6, "", "")
call s:h("String", s:colour2, "", "")
call s:h("Character", s:colour11, "", "")
call s:h("Number", s:colour14, "", "")
call s:h("Boolean", s:colour9, "", "")
call s:h("Float", s:colour13, "", "")

call s:h("Identifier", s:colour5, "", "")
call s:h("Function", s:colour6, "", "")
call s:h("Statement", s:colour13, "", "")

call s:h("Conditional", s:colour13, "", "")
call s:h("Repeat", s:colour10, "", "")
call s:h("Label", s:colour12, "", "")
call s:h("Operator", s:colour6, "", "")
call s:h("Keyword", s:colour9, "", "italic")
call s:h("Exception", s:colour11, "", "")

call s:h("PreProc", s:colour6, "", "")
call s:h("Include", s:colour5, "", "")
call s:h("Define", s:colour9, "", "")
call s:h("Macro", s:colour14, "", "")
call s:h("PreCondit", s:colour12, "", "")

call s:h("Type", s:colour3, "", "")
call s:h("StorageClass", s:colour5, "", "")
call s:h("Structure", s:colour13, "", "")
call s:h("Typedef", s:colour13, "", "")

call s:h("Special", s:colour9, "", "")
call s:h("SpecialChar", s:fg, "", "")
call s:h("Tag", s:fg, "", "")
call s:h("Delimiter", s:colour3, "", "")
call s:h("SpecialComment", s:fg, "", s:comment_fg.attr)
call s:h("Debug", s:fg, "", "")
call s:h("Underlined", s:fg, "", "")
call s:h("Ignore", s:fg, "", "")
call s:h("Error", s:colour1, s:gutter_bg, "")
call s:h("Todo", s:colour5, "", "")

" Teχ
call s:h("texCmdMathEnv", s:colour4, "", "italic")
call s:h("texCmdEnvM", s:colour4, "", "italic")
call s:h("texCmdEnv", s:colour4, "", "italic")
highlight link texEnvMArgName texMathEnvArgName
call s:h("texMathEnvArgName", s:colour14, "", "italic")
call s:h("texEnvMArgName", s:colour14, "", "italic")
call s:h("texEnvArgName", s:colour14, "", "italic")
call s:h("texTabularChar", s:colour3, "", "")
call s:h("texMathSuperSub", s:colour2, "", "")
call s:h("texCmd", s:colour13, "", "italic")
call s:h("texCmdItem", s:colour10, "", "italic")
call s:h("texCmdGreek", s:colour6, "", "italic")
call s:h("texMathSymbol", s:colour12, "", "italic")
" }

" Plugins {
" RainbowCSV
call s:h("RainbowCSV0", s:colour2, "", "")
call s:h("RainbowCSV1", s:colour7, "", "italic")
call s:h("RainbowCSV2", s:colour9, "", "")
call s:h("RainbowCSV3", s:colour6, "", "italic")
call s:h("RainbowCSV4", s:colour13, "", "")
call s:h("RainbowCSV5", s:colour3, "", "italic")
call s:h("RainbowCSV6", s:colour4, "", "")
call s:h("RainbowCSV7", s:colour5, "", "italic")
call s:h("RainbowCSV8", s:colour1, "", "")
call s:h("RainbowCSV9", s:colour14, "", "italic")
call s:h("RainbowCSV10", s:colour10, "", "")
call s:h("RainbowCSV11", s:colour12, "", "italic")
call s:h("RainbowCSV12", s:colour11, "", "")
call s:h("RainbowCSV13", s:colours15, "", "")

" Vim-EasyMotion
call s:h("EasyMotionTarget", s:colour2, "", "italic")
call s:h("EasyMotionTarget2First", s:colour11, "", "italic")
call s:h("EasyMotionTarget2Second", s:colour2, "", "italic")
call s:h("EasyMotionMoveHL", s:colour11, s:colour4, "italic")
call s:h("EasyMotionIncCursor", s:colour0, s:colour6, "")
call s:h("EasyMotionShade", s:comment_fg, "", "")
highlight link EasyMotionIncSearch IncSearch
highlight link EasyMotionSearch Search

" Vim-Searchlight
highlight link Searchlight IncSearch

" Julia-vim
call s:h("juliaFunctionCall", s:colour4, "", "")
call s:h("juliaSemicolon", s:colour12, "", "")
call s:h("juliaColon", s:colour12, "", "")
call s:h("juliaComma", s:colour12, "", "")
call s:h("juliaParDelim", s:colours15, "", "")
call s:h("juliaRangeKeyword", s:colour3, "", "italic")
call s:h("juliaDocString", s:colour6, "", "italic")
call s:h("juliaTodo", s:colour11, "", "bold")
call s:h("juliaParamType", s:colour3, "", "italic")
call s:h("juliaType", s:colour3, "", "italic")
call s:h("juliaConditional", s:colour13, "", "")
call s:h("juliaBlKeyword", s:colour9, "", "italic")

" Python-mode
call s:h("pythonStatement", s:colour13, "", "italic")
call s:h("pythonDecoratorName", s:colour13, "", "italic")
call s:h("pythonExClass", s:colour11, "", "")
call s:h("pythonException", s:colour13, "", "")
call s:h("pythonOperator", s:colour6, "", "")
call s:h("pythonClass", s:colour3, "", "")
call s:h("pythonBuiltinObj", s:colour9, "", "")
call s:h("pythonSelf", s:colour3, "", "italic")
call s:h("pythonBuiltinType", s:colour3, "", "")
call s:h("pythonBuiltinFunc", s:colour4, "", "")
call s:h("pythonFunction", s:colour4, "", "")
call s:h("pythonExtraOperator", s:colour6, "", "")
call s:h("pythonParam", s:colour12, "", "")
call s:h("pythonClassParameters", s:colour12, "", "")
call s:h("pythonInclude", s:colour11, "", "italic")
call s:h("pythonConditional", s:colour5, "", "")
call s:h("pythonStrFormat", s:colour14, "", "")
call s:h("pythonTodo", s:colour11, "", "bold")
call s:h("pythonDocstring", s:colour6, "", "italic")

"Go-vim
call s:h("goDeclType", s:colour12, "", "")
" call s:h("goFloats", s:colour6, "", "")
" call s:h("goSignedInts", s:colour6, "", "")
" call s:h("goUnignedInts", s:colour6, "", "")
call s:h("goFunction", s:colour14, "", "")
call s:h("goBuiltins", s:colour6, "", "")
call s:h("goFunctionCall", s:colour6, "", "")
call s:h("goImport", s:colour9, "", "")
call s:h("goPackage", s:colour9, "", "italic")
call s:h("goDirective", s:colour9, "", "italic")
call s:h("goDeclaration", s:colour6, "", "")
call s:h("goDeclType", s:colour3, "", "")
call s:h("goTypeName", s:colour3, "", "")
call s:h("goType", s:colour3, "", "")
call s:h("goPointerOperator", s:colour6, "", "")
call s:h("goOperator", s:colour6, "", "")
" }

" Vim
call s:h("vimVar", s:colour3, "", "")
call s:h("vimCommand", s:colour6, "", "")
call s:h("vimOption", s:colour11, "", "")
call s:h("vimString", s:colour2, "", "")

" Nim
call s:h("nimKeyword", s:colour3, "", "")
call s:h("nimSpecialVar", s:colour3, "", "")
call s:h("nimBuiltin", s:colour9, "", "")
call s:h("nimFunction", s:colour6, "", "")
call s:h("nimNumber", s:colour11, "", "")
" }

" Git {
call s:h("gitcommitSummary", s:colour9, "", "")
call s:h("gitcommitComment", s:comment_fg, "", s:comment_fg.attr)
call s:h("gitcommitUnmerged", s:colour1, "", "")
call s:h("gitcommitOnBranch", s:fg, "", "")
call s:h("gitcommitBranch", s:colour5, "", "")
call s:h("gitcommitDiscardedType", s:colour1, "", "")
call s:h("gitcommitSelectedType", s:colour2, "", "")
call s:h("gitcommitHeader", s:fg, "", "")
call s:h("gitcommitUntrackedFile", s:colour6, "", "")
call s:h("gitcommitDiscardedFile", s:colour1, "", "")
call s:h("gitcommitSelectedFile", s:colour2, "", "")
call s:h("gitcommitUnmergedFile", s:colour3, "", "")
call s:h("gitcommitFile", s:fg, "", "")
hi link gitcommitNoBranch gitcommitBranch
hi link gitcommitUntracked gitcommitComment
hi link gitcommitDiscarded gitcommitComment
hi link gitcommitSelected gitcommitComment
hi link gitcommitDiscardedArrow gitcommitDiscardedFile
hi link gitcommitSelectedArrow gitcommitSelectedFile
hi link gitcommitUnmergedArrow gitcommitUnmergedFile
" }

" Fix colors in neovim terminal buffers {
  if has('nvim')
    let g:terminal_color_0 = s:colour0.gui
    let g:terminal_color_1 = s:colour1.gui
    let g:terminal_color_2 = s:colour2.gui
    let g:terminal_color_3 = s:colour3.gui
    let g:terminal_color_4 = s:colour4.gui
    let g:terminal_color_5 = s:colour5.gui
    let g:terminal_color_6 = s:colour6.gui
    let g:terminal_color_7 = s:colours15.gui
    let g:terminal_color_8 = s:colour0.gui
    let g:terminal_color_9 = s:colour1.gui
    let g:terminal_color_10 = s:colour2.gui
    let g:terminal_color_11 = s:colour3.gui
    let g:terminal_color_12 = s:colour4.gui
    let g:terminal_color_13 = s:colour5.gui
    let g:terminal_color_14 = s:colour6.gui
    let g:terminal_color_15 = s:colours15.gui
    let g:terminal_color_background = s:bg.gui
    let g:terminal_color_foreground = s:fg.gui
  endif
" }
