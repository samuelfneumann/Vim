" To use this theme with vim-quickui, symlink this file to
" ~/.vim/pack/plugins/start/vim-quickui/colors/quickui/base16.vim
hi! QuickDefaultBackground ctermfg=15 ctermbg=0
hi! QuickDefaultSel cterm=bold ctermfg=0 ctermbg=2
hi! QuickDefaultKey term=bold ctermfg=9
hi! QuickDefaultDisable ctermfg=8
hi! QuickDefaultHelp ctermfg=8
hi! QuickDefaultBorder ctermfg=15 ctermbg=0
hi! QuickDefaultTermBorder ctermfg=15 ctermbg=0

hi! QuickDefaultPreview ctermbg=0

hi! QuickDefaultInput ctermfg=7 ctermbg=4 guifg=#e4e4e4 guibg=#0000a0
hi! QuickDefaultCursor ctermfg=4 ctermbg=7 guifg=#0000a0 guibg=#e4e4e4
hi! QuickDefaultVisual ctermfg=4 ctermbg=15 guifg=#0000a0 guibg=#f4f4f4
