# vim
Configuration files for vim.

## Installation

```bash
cd ~/Documents
git clone --recurse-submodules git@github.com:samuelfneumann/vim.git
cd vim
ln -s vim ~/.vim
ln -s vimrc.vim ~/.vimrc
ln -s vim/colors/quickui/base16.vim ~/.vim/pack/plugins/start/vim-quickui/colors/quickui/base16.vim
```
