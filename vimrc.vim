" Get the OS type
let s:os = trim(system("uname")) " Get the OS name
let s:darwin = s:os==?"Darwin"
let s:linux = s:os==?"Linux"

" Ngraphs --- {{{
" Todo: make this work with operator pending mappings
let g:ngraphs = {
	    \ "sub": "⊂",
	    \ "nsub": "⊄",
	    \ "seq": "⊆",
	    \ "nseq": "⊈",
	    \ "in": "∈",
	    \ "nin": "∉",
	    \ "vep": "ε",
	    \ "ep": "ϵ",
	    \ "bbI": "𝕀",
	    \ "bbi": "𝕚",
	    \ "bbR": "ℝ",
	    \ "bbr": "𝕣",
	    \ "bbE": "𝔼",
	    \ "bbe": "𝕖",
	    \ "bbN": "ℕ",
	    \ "bbn": "𝕟",
	    \ "bbQ": "ℚ",
	    \ "bbq": "𝕢",
	    \ "bbZ": "ℤ",
	    \ "bbz": "𝕫",
	    \ }

function! s:IsPrefix(prefix)
    let ks = keys(g:ngraphs)
    for k in ks
	if stridx(k, a:prefix) >= 0
	    " echo "found " . a:prefix " in " . k
	    return 1
	endif
    endfor
    return -1
endfunction

function! s:Ngraph(k, verbose)
    let k = a:k
    while ! getchar(1)
	let k = k . getcharstr()
	if a:verbose > 0
	    echo k
	endif

	if index(keys(g:ngraphs), k) >= 0
	    return g:ngraphs[k]
	endif

	if s:IsPrefix(k) < 0
	    if a:verbose > 0
		echo "no ngraph with prefix: " . k
	    endif
	    return ""
	endif

    endwhile
endfunction

inoremap <expr> <c-t> <SID>Ngraph("", 1)
cnoremap <expr> <c-t> <SID>Ngraph("", 0)
"}}}


nnoremap <leader>, f,a<cr><esc>

nmap <leader>y "+y
vmap <leader>y "+y
nmap <leader>p "+p
nmap <leader>gp "+gp
nmap <leader>P "+P
nmap <leader>gP "+gP
vmap <leader>p "+p
vmap <leader>gp "+gp
vmap <leader>P "+P
vmap <leader>gP "+gP

" Enable the matchit plugin
runtime macros/matchit.vim

" Functions -----------------------------------------------------------------{{{
" LightOrDark returns if the operating system appearance on macOS is light
" (true) or dark (false)
function LightOrDark()
    " Dynamic colour on macOS
    let l:uname = trim(system("uname"))
    if l:uname==?"Darwin"
	let output =  system("defaults read -g AppleInterfaceStyle")

	" Set if OS appearance is light or dark
	let light_not_dark=1
	if v:shell_error != 0 || output ==? "Light"
	    " Light theme
	    let light_not_dark=1
	else
	    " Dark theme
	    let light_not_dark=0
	endif

	return light_not_dark
    endif

    return 0 " Default to dark
endfunction

"}}}

" Theme options ------------------------------------------------------------{{{
" Set termguicolours so that iTerm uses the fancy gui colours in the terminal
if has('gui_running') || (has('termguicolors') && s:darwin)
    " set termguicolors
    " let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    " let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif

" Set the theme
if s:linux
    colorscheme catppuccin_mocha
elseif s:darwin && LightOrDark()
    colorscheme catppuccin_latte
elseif s:darwin
    colorscheme catppuccin_mocha
endif

" Cursor changes in insert mode to a bar
let dynamic_cursor = 1
if dynamic_cursor && &term =~ "xterm\\|rxvt\\|foot\\|xterm-kitty\\|tmux-256color"

    " let &t_SI = "\<Esc>]12;orange\x7"
    let &t_SI .= "\<Esc>[6 q"
    " let &t_EI = "\<Esc>]12;orange\x7"
    let &t_EI .= "\<Esc>[2 q"
    let &t_SR .= "\<Esc>[4 q"

    " Autocommand to adjust cursor colour in vim
    " No longer using this as I just adjust the cursor colour in the terminal
    " instead
    " augroup cursor
    "     autocmd!
    "     autocmd VimLeave * silent !echo -ne "\033]112\007"
    "     autocmd VimEnter * silent !echo -ne "\033]12;orange\007"
    " augroup end
endif

" }}}

" Misc Commands -------------------------------------------------------------{{{
command W execute 'w !sudo tee % > /dev/null' <bar> edit! " :W sudo saves file

command CheckHighlightUnderCursor echo {l,c,n ->
        \   'hi<'    . synIDattr(synID(l, c, 1), n)             . '> '
        \  .'trans<' . synIDattr(synID(l, c, 0), n)             . '> '
        \  .'lo<'    . synIDattr(synIDtrans(synID(l, c, 1)), n) . '> '
        \ }(line("."), col("."), "name")

command -bang Netrwhist call fzf#run(fzf#wrap('netrw_dirhist',
    \ {'source':
    \ !exists('g:netrw_dirhist_cnt')
    \   ?"tail -n +3 ".g:netrw_home.".netrwhist | cut -d \"'\" -f2- | rev | cut -d \"'\" -f2- | rev | awk '!seen[$0]++'"
    \   :MyUniq(map(range(1,g:netrw_dirhist_cnt), 'g:netrw_dirhist_{v:val}'))
    \ }, <bang>0))

command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis | wincmd p | diffthis
"}}}

" Abbreviations ------------------------------------------------------------{{{
" Contact abbreviations -----------{{{
iabbrev @@ samuelfneumann@gmail.com
iabbrev u@@ sfneuman@ualberta.ca
iabbrev _fullname Samuel Frederick Neumann
iabbrev _fname Samuel
iabbrev _lname Neumann
iabbrev _mname Frederick
"}}}

iabbrev _# ####################################################################

" Incorrect spelling abbreviations -----------------------------------------{{{
iabbrev tahn than
iabbrev parallle parallel
iabbrev staet state
iabbrev satte state
iabbrev gamam gamma
iabbrev acotr actor
iabbrev adn and
iabbrev aer are
iabbrev browswer browser
iabbrev brwoser browser
iabbrev bsae base
iabbrev cahced cached
iabbrev chaced cached
iabbrev corss cross
iabbrev discoutn discount
iabbrev edn end
iabbrev eerror error
iabbrev epsilong epsilon
iabbrev erorr error
iabbrev eys yes
iabbrev functoin function
iabbrev geredy greedy
iabbrev hrad hard
iabbrev hwere where
iabbrev hwile while
iabbrev ingore ignore
iabbrev instrucotr instructor
iabbrev jluia julia
iabbrev juila julia
iabbrev julai julia
iabbrev julai julia
iabbrev julai julia
iabbrev laerning learning
iabbrev learnign learning
iabbrev lfag flag
iabbrev macor macro
iabbrev matser master
iabbrev msater master
iabbrev nto not
iabbrev od do
iabbrev ot to
iabbrev paln plan
iabbrev Pythong Python
iabbrev pythong python
iabbrev retrun return
iabbrev scael scale
iabbrev sceicne science
iabbrev sceince science
iabbrev sciecne science
iabbrev sclae scale
iabbrev seput setup
iabbrev seupt setup
iabbrev sfot soft
iabbrev teh the
iabbrev tihs this
iabbrev wheer where
iabbrev wihle while
"}}}
"}}}

" Digraphs -----------------------------------------------------------------{{{
" Greek letters
:exe 'digr ne ' . 0x2288
:exe 'digr se ' . 0x2286
:exe 'digr ns ' . 0x2284
:exe 'digr su ' . 0x2282
:exe 'digr ni ' . 0x2209
:exe 'digr in ' . 0x2208
:exe 'digr al ' . 0x03b1
:exe 'digr Al ' . 0x0391
:exe 'digr be ' . 0x03b2
:exe 'digr Be ' . 0x0392
:exe 'digr ga ' . 0x03b3
:exe 'digr Ga ' . 0x0393
:exe 'digr de ' . 0x03b4
:exe 'digr De ' . 0x0394
:exe 'digr ep ' . 0x03f5
:exe 'digr Ep ' . 0x0395
:exe 'digr ve ' . 0x03b5
:exe 'digr ze ' . 0x03b6
:exe 'digr Ze ' . 0x0396
:exe 'digr et ' . 0x03b7
:exe 'digr Et ' . 0x0397
:exe 'digr th ' . 0x03b8
:exe 'digr Th ' . 0x0398
:exe 'digr io ' . 0x03b9
:exe 'digr Io ' . 0x0399
:exe 'digr ka ' . 0x03ba
:exe 'digr Ka ' . 0x039a
:exe 'digr la ' . 0x03bb
:exe 'digr La ' . 0x039b
:exe 'digr mu ' . 0x03bc
:exe 'digr Mu ' . 0x039c
:exe 'digr nu ' . 0x03bd
:exe 'digr Nu ' . 0x039d
:exe 'digr xi ' . 0x03be
:exe 'digr Xi ' . 0x039e
:exe 'digr om ' . 0x03bf
:exe 'digr Om ' . 0x039f
:exe 'digr pi ' . 0x03c0
:exe 'digr Pi ' . 0x03a0
:exe 'digr rh ' . 0x03c1
:exe 'digr Rh ' . 0x03a1
:exe 'digr si ' . 0x03c3
:exe 'digr Si ' . 0x03a3
:exe 'digr vs ' . 0x03c2
:exe 'digr ta ' . 0x03c4
:exe 'digr Ta ' . 0x03a4
:exe 'digr up ' . 0x03c5
:exe 'digr Up ' . 0x03a5
:exe 'digr ph ' . 0x03c6
:exe 'digr Ph ' . 0x03a6
:exe 'digr ch ' . 0x03c7
:exe 'digr Ch ' . 0x03a7
:exe 'digr ps ' . 0x03c8
:exe 'digr Ps ' . 0x03a8
:exe 'digr om ' . 0x03c9
:exe 'digr Om ' . 0x03a9
:exe 'digr na ' . 0x2207
:exe 'digr pd ' . 0x2202
"}}}

" General Settings ---------------------------------------------------------{{{
" Leaders {{{
nnoremap <space> <nop>
let mapleader=" "
let maplocalleader="-"
tnoremap <space> <space>
"}}}

" Don't use vi compatability
set nocompatible

set conceallevel=2

" Wild{menu,char,mode}
set wildchar=<tab>
set wildcharm=<c-z>
set wildmenu
set wildmode=longest:full,full
set wildignorecase

" Reload file when changed outside of vim
autocmd FileChangedShell * ++nested edit

set fillchars+=vert:│

cnoremap <expr><c-l> pumvisible() ? "\<right>" : "\<c-l>"
" cnoremap <expr><cr> pumvisible() ? "\<right>" : "\<cr>"

" Confirm before closing un-saved files
set confirm

" When joining lines with J, insert a single line after a . ! or ?.
set nojoinspaces

" Encodings to try when opening file
set fileencodings=utf-8,ucs-bom,latin1

" The encoding to use to type and display
set termencoding=utf-8

" Encoding to use inside of Vim (e.g. in buffers)
set encoding=utf-8

" Set filename in window title bar
set title

" Show partial commands you type in last line
set showcmd

" Don't show mode in last line, we use lightline
set noshowmode

" set mouse=a " Allow mouse usage

" Increase scroll speed
set ttyfast

" Use magic for escape characters
set magic

" Scroll 5 lines past cursor with mouse
set scrolloff=5

set backspace=indent,eol,start " Allow backspacing over auto indents etc.

" Timeout for next-key-press in maps
set timeoutlen=350

" Write swap file only after half a second of inactivity
set updatetime=500

set clipboard=unnamed,unnamedplus

" Set how omnicomplete works
set omnifunc=syntaxcomplete#Complete

" Set the thesaurus file
set thesaurus=/home/samuel/.vim/mthesaur.txt

" Set autocomplete options
set completeopt=longest,menuone,preview,popup
if has("patch-8.1.1904")
    set completepopup=align:menu,border:off,highlight:Pmenu
endif
set complete+=.,k,w,b

set shortmess+=a
set shortmess+=I

set infercase

" Enable filetype plugins
filetype plugin on

" Delay for popups
set balloondelay=200

" Allow the sign column for quickfixes
set signcolumn=number

" Wrap long lines
set wrap
set linebreak
" }}}

" Plugins ------------------------------------------------------------------{{{

" Option toggling ----------------------------------------------------------{{{
" Map key to toggle opt
function MapToggle(key, opt)
  let cmd = ':set '.a:opt.'! \| set '.a:opt."?\<CR>"
  exec 'nnoremap '.a:key.' '.cmd
  " exec 'inoremap '.a:key." \<C-O>".cmd
endfunction
command -nargs=+ MapToggle call MapToggle(<f-args>)

function! FoldColumnToggle()
    if &foldcolumn
        setlocal foldcolumn=0
    else
        setlocal foldcolumn=6
    endif
endfunction

" Display-altering option toggles
MapToggle <F1> hlsearch
MapToggle \h hlsearch

MapToggle <F2> wrap
MapToggle \w wrap

MapToggle <F3> list
MapToggle \l list

MapToggle <F4> number
MapToggle \n number

MapToggle <F5> relativenumber
MapToggle \r relativenumber

MapToggle <F6> spell
MapToggle \S spell

" Behavior-altering option toggles
" map <silent> <F10> :set invpaste<cr>
MapToggle <F8> ignorecase
MapToggle \I ignorecase

MapToggle <F9> foldenable
MapToggle \f foldenable

MapToggle <F10> paste
set pastetoggle=<F10>
MapToggle \p paste

MapToggle <F11> cursorbind
MapToggle \c cursorbind

MapToggle <F12> scrollbind
MapToggle \s scrollbind

" Conceal toggling
function! ToggleConcealLevel()
    if &conceallevel == 0
        setlocal conceallevel=2
    else
        setlocal conceallevel=0
    endif
endfunction

nnoremap \C :call ToggleConcealLevel()<CR>
"}}}

" Folding ------------------------------------------------------------------{{{
set nofoldenable
augroup Folding
    autocmd!
    autocmd FileType julia setlocal foldmethod=syntax
    autocmd FileType python setlocal foldmethod=syntax
    autocmd FileType vim setlocal foldenable
augroup end
let g:pymode_folding = 1
set foldcolumn=0
" }}}

" Undo files ---------------------------------------------------------------{{{
" Create the undo directory
if !isdirectory($HOME."/.vim")
    call mkdir($HOME."/.vim", "", 0770)
endif
if !isdirectory($HOME."/.vim/undo-dir")
    call mkdir($HOME."/.vim/undo-dir", "", 0700)
endif

set undodir=~/.vim/undo-dir
set undofile

" You may want to add this to your crontab to ensure that the vim undo files
" get cleaned up after 90 days
" # m  h  dom mon dow   command
"   00 00 *   *   3     find /home/samuel/.vim/undo-dir -type f -mtime +90 -delete
"}}}

" Terminal settings --------------------------------------------------------{{{
augroup TerminalSettings
    autocmd!
    autocmd BufEnter * if &buftype==?"terminal" | setlocal nospell | endif
augroup end
" }}}

" Bracket matching ---------------------------------------------------------{{{
set matchpairs+=<:> " Highlight these kinds of brackets as well
set showmatch " Show matching brackets when cursor is over them

let g:rainbow_active = 1 " For rainbow brackets plugin
let g:rainbow_conf = {
\   'ctermfgs': [3, 4, 13, 12, 11, 9, 10, 5, 3, 6],
\   'parentheses': ['start=/(/ end=/)/ fold', 'start=/\[/ end=/\]/ fold',
\		    'start=/{/ end=/}/ fold', 'start=/</ end=/>/ fold']
\}
"}}}
"
" vim-markdown --------------------------------------------------------------{{{
let g:vim_markdown_math = 1
let g:vim_markdown_folding_style_pythonic=0
" }}}

" Searching ----------------------------------------------------------------{{{
set hlsearch " Use highlighting when searching
set incsearch " Highlight matching characters as you type
set shortmess-=S " Show the number of search matches at the bottom right
nnoremap <leader>nhs :nohlsearch<cr>

" Use very-magic searching by default, and remap search maps without very-magic
nnoremap / /\v
nnoremap ./ /
nnoremap ? ?\v
nnoremap .? ?
nnoremap >? ?
vnoremap / /\v
vnoremap ./ /
vnoremap ? ?\v
vnoremap .? ?
vnoremap >? ?

" Ignore case when search term contains only lowercase letters. Only take case
" into account when the search term has uppercase letters.
set ignorecase
set smartcase " Use case insensitive search unless capital in search pattern

" fzf.vim and Searching ----------------------------------------------------{{{
" https://dev.to/iggredible/how-to-search-faster-in-vim-with-fzf-vim-36go
set rtp+=~/.fzf
let g:fzf_layout = { 'down': '35%'}
set grepprg=rg\ --vimgrep\ --smart-case\ --follow\ --no-column

function! s:build_quickfix_list(lines)
  call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
  copen
  cc
endfunction

let g:fzf_action = {
    \ 'ctrl-q': function('s:build_quickfix_list'),
    \ 'ctrl-t': 'tab split',
    \ 'ctrl-x': 'split',
    \ 'ctrl-v': 'vsplit'}

let g:fzf_colors =
    \ { 'fg':    ['fg', 'Normal'],
    \ 'bg':      ['bg', 'Normal'],
    \ 'hl':      ['fg', 'Comment'],
    \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
    \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
    \ 'hl+':     ['fg', 'Keyword'],
    \ 'info':    ['fg', 'PreProc'],
    \ 'border':  ['fg', 'Ignore'],
    \ 'prompt':  ['fg', 'Conditional'],
    \ 'pointer': ['fg', 'Exception'],
    \ 'marker':  ['fg', 'Keyword'],
    \ 'spinner': ['fg', 'Label'],
    \ 'header':  ['fg', 'Comment'] }

" List files
nnoremap <silent> <leader>ff :FZFFiles<cr>
nnoremap <silent> <leader>fg :FZFGFiles<cr>
tnoremap <silent> <c-w>ff <c-w>:FZFFiles<cr>
tnoremap <silent> <c-w>fg <c-w>:FZFGFiles<cr>

" Arglist
nnoremap <silent> <leader>fa :FZFArgs<cr>
tnoremap <silent> <c-w>fa <c-w>:FZFArgs<cr>

" List commits in the current buffer and all buffers
nnoremap <silent> <leader>fcb :FZFBCommits<cr>
nnoremap <silent> <leader>fcc :FZFCommits<cr>
tnoremap <silent> <c-w>fcb <c-w>:FZFBCommits<cr>
tnoremap <silent> <c-w>fcc :<c-w>FZFCommits<cr>

" List buffers and tabs
nnoremap <silent> <leader>fbs :Buffers<cr>
nnoremap <silent> <leader>fbd :BDelete<cr>
nnoremap <silent> <leader>fw :FZFWindows<cr>
tnoremap <silent> <c-w>fbs <c-w>:Buffers<cr>
tnoremap <silent> <c-w>fbd <c-w>:BDelete<cr>
tnoremap <silent> <c-w>fw <c-w>:FZFWindows<cr>

" Search lines in current buffer and in all buffers
nnoremap <silent> <leader>flb :FZFBLines<cr>
nnoremap <silent> <leader>fll :FZFLines<cr>
tnoremap <silent> <c-w>flb <c-w>:FZFBLines<cr>
tnoremap <silent> <c-w>fll <c-w>:FZFLines<cr>

" Search tags in current buffer and in all buffers
nnoremap <silent> <leader>ftb :BTags<cr>
nnoremap <silent> <leader>ftt :Tags<cr>

" Search through marks
nnoremap <silent> <leader>fm :FZFMarks<cr>
tnoremap <silent> <c-w>fm <c-w>:FZFMarks<cr>

nnoremap <silent> <leader>frg :FZFRg<cr>
tnoremap <silent> <c-w>frg <c-w>:FZFRg<cr>

" Mapping selecting mappings
nmap <leader><tab> <plug>(fzf-maps-n)
xmap <leader><tab> <plug>(fzf-maps-x)
omap <leader><tab> <plug>(fzf-maps-o)

" Insert mode completion
imap <c-f><c-p> <plug>(fzf-complete-word)
imap <c-f><c-f> <plug>(fzf-complete-path)
imap <c-f><c-l> <plug>(fzf-complete-line)
inoremap <expr> <c-f><c-k> fzf#vim#complete('cat ' . &dictionary)

" Global line completion (not just open buffers. ripgrep required)
inoremap <expr> <c-f><c-g> fzf#vim#complete(fzf#wrap({
  \ 'prefix': '^.*$',
  \ 'source': 'rg -n ^ --color always',
  \ 'options': '--ansi --delimiter : --nth 3..',
  \ 'reducer': { lines -> join(split(lines[0], ':\zs')[2:], '') }}))

" FZF Commands -------------------------------------------------------------{{{
" Helper Function ----------------------------------------------------------{{{
let s:TYPE = {'dict': type({}), 'funcref': type(function('call')), 'string': type(''), 'list': type([])}

function FZFPreview(bang, ...) abort
    let preview_args = get(g:, 'fzf_preview_window', ['right:50%', 'ctrl-/'])
    if empty(preview_args)
        return { 'options': ['--preview-window', 'hidden'] }
    endif
    return call('fzf#vim#with_preview', extend(copy(a:000), preview_args))
endfunction

function! s:function(name)
    return function(a:name)
endfunction

function! s:extend_opts(dict, eopts, prepend)
  if empty(a:eopts)
    return
  endif
  if has_key(a:dict, 'options')
    if type(a:dict.options) == s:TYPE.list && type(a:eopts) == s:TYPE.list
      if a:prepend
        let a:dict.options = extend(copy(a:eopts), a:dict.options)
      else
        call extend(a:dict.options, a:eopts)
      endif
    else
      let all_opts = a:prepend ? [a:eopts, a:dict.options] : [a:dict.options, a:eopts]
      let a:dict.options = join(map(all_opts, 'type(v:val) == s:TYPE.list ? join(map(copy(v:val), "fzf#shellescape(v:val)")) : v:val'))
    endif
  else
    let a:dict.options = a:eopts
  endif
endfunction

function! s:wrap(name, opts, bang)
  " fzf#wrap does not append --expect if sink or sink* is found
  let opts = copy(a:opts)
  let options = ''
  if has_key(opts, 'options')
    let options = type(opts.options) == s:TYPE.list ? join(opts.options) : opts.options
  endif
  if options !~ '--expect' && has_key(opts, 'sink*')
    let Sink = remove(opts, 'sink*')
    let wrapped = fzf#wrap(a:name, opts, a:bang)
    let wrapped['sink*'] = Sink
  else
    let wrapped = fzf#wrap(a:name, opts, a:bang)
  endif
  return wrapped
endfunction

function! s:merge_opts(dict, eopts)
  return s:extend_opts(a:dict, a:eopts, 0)
endfunction

function! s:fzf(name, opts, extra)
  let [extra, bang] = [{}, 0]
  if len(a:extra) <= 1
    let first = get(a:extra, 0, 0)
    if type(first) == s:TYPE.dict
      let extra = first
    else
      let bang = first
    endif
  elseif len(a:extra) == 2
    let [extra, bang] = a:extra
  else
    throw 'invalid number of arguments'
  endif

  let extra  = copy(extra)
  let eopts  = has_key(extra, 'options') ? remove(extra, 'options') : ''
  let merged = extend(copy(a:opts), extra)
  call s:merge_opts(merged, eopts)
  return fzf#run(s:wrap(a:name, merged, bang))
endfunction

function! s:find_open_window(b)
  let [tcur, tcnt] = [tabpagenr() - 1, tabpagenr('$')]
  for toff in range(0, tabpagenr('$') - 1)
    let t = (tcur + toff) % tcnt + 1
    let buffers = tabpagebuflist(t)
    for w in range(1, len(buffers))
      let b = buffers[w - 1]
      if b == a:b
        return [t, w]
      endif
    endfor
  endfor
  return [0, 0]
endfunction

function! s:jump(t, w)
  execute a:t.'tabnext'
  execute a:w.'wincmd w'
endfunction
" }}}

" Buffers ------------------------------------------------------------------{{{
function! s:format_buffer(b)
  let l:name = bufname(a:b)
  let l:name = empty(l:name) ? '[No Name]' : fnamemodify(l:name, ":p:~:.")
  let l:flag = a:b == bufnr('')  ? '%' :
	  \ (a:b == bufnr('#') ? '#' : ' ')
  let l:modified = getbufvar(a:b, '&modified') ? ' [+]' : ''
  let l:readonly = getbufvar(a:b, '&modifiable') ? '' : ' [RO]'
  let l:extra = join(filter([l:modified, l:readonly], '!empty(v:val)'), '')
  return substitute(printf("[%s] %s\t%s\t%s", a:b, l:flag, l:name, l:extra), '^\s*\|\s*$', '', 'g')
endfunction

" BDelete ------------------------------------------------------------------{{{
function! s:bufdel(lines)
  if len(a:lines) < 2
    return
  endif
  let b = matchstr(a:lines[1], '\[\zs[0-9]*\ze\]')
  if empty(a:lines[0]) && get(g:, 'fzf_buffers_jump')
    let [t, w] = s:find_open_window(b)
    if t
      call s:jump(t, w)
      return
    endif
  endif
  execute 'bdelete' b
endfunction

function! s:delete_buffers(...)
  let [query, args] = (a:0 && type(a:1) == type('')) ? [a:1, a:000[1:]] : ['', a:000]

  let sorted = fzf#vim#_buflisted_sorted()
  let header_lines = '--header-lines=' . (bufnr('') == get(sorted, 0, 0) ? 1 : 0)
  let tabstop = len(max(sorted)) >= 4 ? 9 : 8
  return s:fzf('buffers', {
  \ 'source':  map(sorted, 'fzf#vim#_format_buffer(v:val)'),
  \ 'sink*':   s:function('s:bufdel'),
  \ 'options': ['+m', '-x', '--tiebreak=index', header_lines, '--ansi', '-d', '\t', '--with-nth', '3..', '-n', '2,1..2', '--prompt', 'Delete> ', '--preview-window', '+{2}-/2', '--tabstop', tabstop]
  \}, args)
endfunction

command! -bar -bang -nargs=? -complete=buffer BDelete call s:delete_buffers(<q-args>, fzf#vim#with_preview({ "placeholder": "{1}" }), <bang>0)

" }}}
" }}}

" Args ---------------------------------------------------------------------{{{
command! -bang Args call fzf#run(fzf#wrap('args',
    \ {'source': map([argidx()]+(argidx()==0?[]:range(argc())[0:argidx()-1])+range(argc())[argidx()+1:], 'argv(v:val)')}, <bang>0))

command! -bang FZFArgs call fzf#run(fzf#wrap('args', FZFPreview(<bang>0, {'source': map([argidx()]+(argidx()==0?[]:range(argc())[0:argidx()-1])+range(argc())[argidx()+1:], 'argv(v:val)')})))
" }}}

" Ensure Rg doesn't search for filenames
command! -bang -nargs=* Rg call fzf#vim#grep("rg --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 1, {'options': '--delimiter : --nth 4..'}, <bang>0)

" Locate -------------------------------------------------------------------{{{
command! -nargs=1 -bang Locate call fzf#run(fzf#wrap(
      \ {'source': 'locate <q-args>', 'options': '-m'}, <bang>0))

command! -nargs=1 -bang FZFLocate call fzf#run(fzf#wrap(fzf#vim#with_preview(FZFPreview(<bang>0, {'source': 'locate <q-args>', 'options': '-m'})))
"}}}

" }}}
" }}}

"}}}

" Syntax highlighting and line numbers -------------------------------------{{{
syntax on
set number
"}}}

" Auto read when files are changed outside vim -----------------------------{{{
set autoread
" silent! here ensure no wierdness happens when ctrl-f is pressed in command
" mode.
au CursorHold,FocusGained,BufEnter * silent! checktime
""}}}

" On Save -----------------------------------------------------------------{{{
function RemoveTrailingWhitespace()
    execute "normal! mq"
    execute "normal! :%s/\\s\\+$//e\r"
    execute "normal! `q"
endfunction

function RemoveFinalBlankLine()
    execute "normal! mq"
    execute "normal! G"
    let ln = getline(".")
    if ln ==# ""
	execute "normal! dd"
    endif
    execute "normal! `q"
endfunction

function RemoveFirstNBlankLines()
    execute "normal! mc"
    execute "normal! gg"
    let ln = getline(".")
    while ln ==# ""
	execute "normal! dd"
	let ln = getline(".")
    endwhile
    execute "normal! `c"
endfunction

augroup OnSave
    " Autocommand group for events to happen on save
    autocmd!
    autocmd BufWritePre * call RemoveTrailingWhitespace()
    " autocmd BufWritePre * call RemoveFinalBlankLine()
    " autocmd BufWritePre * call RemoveFirstNBlankLines()
augroup end

" Remove trailing whitespace, leaving cursor in-place
nnoremap <leader><space><space>x mq:s/\v\s+$//e<cr>:nohlsearch<cr>`q
nnoremap <leader><space>x mq:%s/\v\s+$//e<cr>:nohlsearch<cr>`q
vnoremap <leader><space>x mq:s/\v\s+$//e<cr>:nohlsearch<cr>`q
" }}}

" Ruler/StatusLine/Lightline ---------------------------------------------- {{{
" If using lightline, these setting are overridden after this file runs. To
" keep these default settings, remove lightline
set ruler
set rulerformat=%Y\ %=(%l,%c)%V%p%% " Disappears if statusline is on

" Lightline settings -------------------------------------------------------{{{
" If weird things happen, place this before setting the colourscheme
if s:darwin
    if LightOrDark()
	let s:colours = 'catppuccin_latte'
    else
	let s:colours = 'catppuccin_mocha'
    endif
elseif has('termguicolors') && s:linux && &termguicolors
    let s:colours = 'catppuccin_mocha'
else
    let s:colours = 'catppuccin_mocha'
endif

function! MyMode()
  if &paste != get(s:, 'paste')
    let s:paste = &paste
    call lightline#update()
  endif
  return lightline#mode()
endfunction

function! MyPaste()
  return &paste ? "PASTE" : ""
endfunction

" Git-Gutter -------------------------------------------------------------- {{{
function! GitGutterStatus()
  let [a,m,r] = GitGutterGetHunkSummary()
  return printf('+%d ~%d -%d', a, m, r)
endfunction

function! GitGutterStatusAdd()
  let [a,m,r] = GitGutterGetHunkSummary()
  return printf('+%d', a)
endfunction

function! GitGutterStatusModify()
  let [a,m,r] = GitGutterGetHunkSummary()
  return printf('~%d', m)
endfunction

function! GitGutterStatusRemove()
  let [a,m,r] = GitGutterGetHunkSummary()
  return printf('-%d', r)
endfunction
" }}}

" Vim-Signify ------------------------------------------------------------- {{{
function! GitSignifyStatus()
  let [a,m,r] = sy#repo#get_stats()
  return printf('%d %d %d', a, m, r)
endfunction

function! GitSignifyStatusAdd()
  let [a,m,r] = sy#repo#get_stats()
  if a == -1
      return "-"
  else
      return printf('+%d', a)
  endif

endfunction

function! GitSignifyStatusModify()
  let [a,m,r] = sy#repo#get_stats()
  if m == -1
      return "-"
  else
      return printf('~%d', m)
  endif
endfunction

function! GitSignifyStatusRemove()
  let [a,m,r] = sy#repo#get_stats()
  if r == -1
      return "-"
  else
      return printf('-%d', r)
  endif
endfunction
" }}}

function! GetGitBranch()
    let branch = FugitiveHead()
    if branch ==# ""
	return "-"
    else
	return "" . branch
    endif
endfunction

let g:lightline = {
	    \ 'colorscheme': s:colours,
	    \ 'active': {
		\ 'left': [ ['mode', 'paste'],
		\	    ['readonly', 'filename' ],
		\	    ['obsession'],
		\	    ['modified']
		\ ],
		\ 'right': [ [ 'lineinfo' ],
		\            [ 'bufwin', 'percent' ],
		\            [ 'filetype' ],
		\            [ 'fileencoding' ],
		\	     [ 'fileformat' ],
		\	     [ 'git-signify-rm' ],
		\	     [ 'git-signify-mod' ],
		\	     [ 'git-signify-add' ],
		\            [ 'fugitive' ],
		\ ]
		\ },
	    \ 'inactive': {
		\ 'left': [ ['mode', 'paste'],
		\	    ['readonly', 'filename' ],
		\	    ['obsession'],
		\	    ['modified']
		\ ],
		\ 'right': [ [ 'lineinfo' ],
		\            [ 'bufwin', 'percent' ],
		\            [ 'filetype' ],
		\            [ 'fileencoding' ],
		\	     [ 'fileformat' ],
		\	     [ 'git-signify-rm' ],
		\	     [ 'git-signify-mod' ],
		\	     [ 'git-signify-add' ],
		\            [ 'fugitive' ],
		\ ]
		\ },
		\ 'component': {
		\ },
		\ 'component_function': {
		    \ 'mode': 'MyMode',
		\ },
		\ 'component_expand': {
		    \ 'paste': 'MyPaste',
		\ },
		\ 'component_type': {
		    \ 'paste': 'warning',
		\ }
\ }

let g:lightline.component = {
    \ 'mode': '%{lightline#mode()}',
    \ 'absolutepath': '%F',
    \ 'relativepath': '%f',
    \ 'filename': '%t',
    \ 'modified': '%M',
    \ 'bufnum': '%n',
    \ 'paste': '%{&paste?"PASTE":""}',
    \ 'readonly': '%R',
    \ 'charvalue': '%b',
    \ 'charvaluehex': '%B',
    \ 'fileencoding': '%{&fenc!=#""?&fenc:&enc}',
    \ 'fileformat': '%{&ff}',
    \ 'filetype': '%{&ft!=#""?&ft:"no ft"}',
    \ 'percent': '%3p%%',
    \ 'percentwin': '%P',
    \ 'spell': '%{&spell?&spelllang:""}',
    \ 'lineinfo': '%3l:%-2c',
    \ 'line': '%l',
    \ 'column': '%c',
    \ 'close': ' × ',
    \ 'bufwin': '%n:%{winnr()}',
    \ 'obsession': '%{ObsessionStatus("[🖪]")}',
    \ 'fugitive': '%{GetGitBranch()}',
    \ 'git-gutter': '%{GitGutterStatus()}',
    \ 'git-gutter-add': '%{GitGutterStatusAdd()}',
    \ 'git-gutter-mod': '%{GitGutterStatusModify()}',
    \ 'git-gutter-rm': '%{GitGutterStatusRemove()}',
    \ 'git-signify': '%{GitSignifyStatus()}',
    \ 'git-signify-add': '%{GitSignifyStatusAdd()}',
    \ 'git-signify-mod': '%{GitSignifyStatusModify()}',
    \ 'git-signify-rm': '%{GitSignifyStatusRemove()}',
    \ 'winnr': '%{winnr()}' }

" Enable the lightline status and tab lines
let g:lightline.enable = {
	    \ 'statusline': 1,
	    \ 'tabline': 1
	    \ }

" Set the default separator
let g:lightline.separator = { 'left': '', 'right': '' }
let g:lightline.subseparator = { 'left': '', 'right': '' }

" Set the default tabline spearator
let g:lightline.tabline_subseparator = g:lightline.subseparator
let g:lightline.tabline_separator = g:lightline.separator
" }}}

" Status line --------------------------------------------------------------{{{
" Function: Colour statusLine based on macOS appearance --------------------{{{
" SetStatusLineColour sets the status line theme based on the appearance of
" macOS - light or dark. If not on macOS, then the dark theme is used by
" default.
function SetStatusLineColour()
    hi clear StatusLine " Clear status line of current window
    hi clear StatusLineNC " Clear status line of non-current window
    hi clear StatusLineTerm " Clear status line of current terminal
    hi clear StatusLineTermNC " Clear status line of non-current terminal

    let light_not_dark = LightOrDark()
    if light_not_dark
	hi StatusLine cterm=none gui=none term=none ctermbg=darkgray
		    \ ctermfg=white guibg=darkgray guifg=white
	hi StatusLineNC cterm=none gui=none term=none ctermbg=gray
		    \ ctermfg=white guibg=gray guifg=white

	"Manually set the statusline for the terminal, since still in beta in
	" vim 8.1
	hi StatusLineTerm cterm=none gui=none term=none ctermbg=darkgreen
		    \ ctermfg=white guibg=darkgreen guifg=white
	hi StatusLineTermNC cterm=none gui=none term=none ctermbg=green
		    \ ctermfg=white guibg=green guifg=white
	return
    else
	hi StatusLine cterm=none gui=none term=none ctermbg=gray ctermfg=black
		    \ guibg=gray guifg=black
	hi StatusLineNC cterm=none gui=none term=none ctermbg=darkgray
		    \ ctermfg=black guibg=darkgray guifg=black

	"Manually set the statusline for the terminal, since still in beta in
	" vim 8.1
	hi StatusLineTerm cterm=none gui=none term=none ctermbg=blue
		    \ ctermfg=white guibg=blue guifg=white
	hi StatusLineTermNC cterm=none gui=none term=none ctermbg=darkblue
		    \ ctermfg=white guibg=darkblue guifg=white
	return
    endif
endfunction
"}}}
" If lightline is disabled, set the statusline
if !g:lightline.enable.statusline
    " Theme the statusline
    if g:colors_name ==? "default"
	call SetStatusLineColour()
    endif

    let g:currentmode = {
		\ 'n' : 'NORMAL',
		\ 'i' : 'INSERT',
		\ 'r' : 'REPLACE',
		\ 'R' : 'REPLACE',
		\ 'Rv' : 'REPLACE',
		\ 'v' : 'VISUAL',
		\ 'V' : 'V-LINE',
		\ "\<C-v>": 'V-BLOCK',
		\ 'c' : 'COMMAND',
		\ 's' : 'SELECT',
		\ 'S' : 'S-LINE',
		\ "\<C-s>": 'S-BLOCK',
		\ 't': 'TERMINAL',
		\ }

    set statusline=
    set statusline+=\ %n	" buffer number

    set statusline+=\ ==%{toupper(g:currentmode[mode()])}==" mode
    set statusline+=\ %{&ff}	" file format
    set statusline+=%y		" file type
    set statusline+=\ %<%F	" full path
    set statusline+=%-5m	" modified flag
    set statusline+=%V		" right align
    set statusline+=%=%5l	" current line
    set statusline+=/%L		" total lines
    set statusline+=%4v\	" virtual column number
endif

" Show the statusline
set laststatus=2
" }}}

" }}}

" TabLine ------------------------------------------------------------------{{{

" Function: Colour tabline based on macOS appearance ------------------------{{{
" SetTabLine sets the tab line theme based on the theme of macOS - light or
" dark. If not on macOS, then the dark theme is used by default
function SetTabLine()
    hi clear TabLine
    hi clear TabLineFill
    hi clear TabLineSel

    let light_not_dark = LightOrDark()
    if light_not_dark
	hi TabLine cterm=none term=none gui=none ctermbg=darkgray ctermfg=white
		    \ guibg=darkgray guifg=white
	hi TabLineFill ctermbg=darkgray guibg=darkgray
	hi TabLineSel cterm=bold term=bold gui=bold ctermfg=black
	return
    else
	hi TabLine cterm=none term=none gui=none ctermbg=gray ctermfg=black
		    \ guibg=gray guifg=black
	hi TabLineFill ctermbg=gray guibg=gray
	hi TabLineSel cterm=bold term=bold gui=bold ctermfg=white
	return
    endif
endfunction
" }}}

if !g:lightline.enable.tabline && g.colors_name ==? "default"
    call SetTabLine()
endif

"}}}

" Cursorline -------------------------------------------------------{{{
if g:colors_name ==? "default"
    hi clear CursorLine
    highlight CursorLineNR cterm=bold term=bold gui=bold ctermbg=8 guibg=8
    highlight CursorLine cterm=bold term=bold gui=bold ctermbg=8 guibg=8
endif
set cursorline
"}}}

" Tabbing ------------------------------------------------------------------{{{

" Sets the tab levels for nim files
function SetNimTabbing()
    set tabstop=8
    set softtabstop=2
    set shiftwidth=2 " Set shift width to 2 spaces
endfunction

set hidden
set noexpandtab
set smarttab
set tabstop=8
set softtabstop=4
set shiftwidth=4 " Set shift width to 4 spaces
set autoindent
set smartindent
augroup ModifiedExpandTab
    autocmd!
    autocmd FileType python setlocal expandtab " PEP8 says to expand tabs
    autocmd FileType julia setlocal expandtab " Julia should have expanded tabs

    " Really, the nim.vim extension handles this, but explicitly set these
    " values in case the nim.vim extension is not installed
    autocmd FileType nim setlocal expandtab
    autocmd FileType nim call SetNimTabbing()
augroup end
"}}}

" File Type Detection ------------------------------------------------------{{{
augroup FiletypeVim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup end
filetype on
filetype plugin on " Use only plugins for this file's type
filetype indent on " Load an indent file for the detected file type
"}}}

" Textwidth ----------------------------------------------------------------{{{
set textwidth=79
augroup ModifiedTextWidth
    " Git commit body should be only 72 characters long
    autocmd!
    autocmd FileType gitcommit setlocal textwidth=72

    " Julia text width is 92 characters
    autocmd FileType julia setlocal textwidth=92

    " LaTeχ can have longer text width
    autocmd FileType tex setlocal textwidth=120
augroup end
"}}}

" Format options -----------------------------------------------------------{{{
set formatoptions+=t " Auto wrap text using textwidth
set formatoptions+=c " Auto wrap comments using textwidth
set formatoptions+=r " Automatically insert comment leader on <ENTER>
set formatoptions+=n " Recognize numbered lists when formatting
set formatoptions-=l " Long lines should be broken in insert mode
set formatoptions+=j " Remove comment leader when joining lines
set formatoptions+=q " Allow formatting of comments with "gq"
"}}}

" Colorcolumn --------------------------------------------------------------{{{
" if g:colors_name ==? "default"
"     highlight ColorColumn ctermbg=cyan guibg=cyan
" endif
set colorcolumn=+1
"}}}

" Julia-vim Options --------------------------------------------------------{{{
" " Manually control LaTeΧ to Unicode if you want completefunc to work properly
" " in this case, uncomment these lines and comment out the blacklist line
" " below
" function! LaTeXUnicodeOff()
"     if &ft ==# "tex"
"         call LaTeXtoUnicode#Disable()
"     elseif &ft ==# "snippets"
"         call LaTeXtoUnicode#Disable()
"     endif
" endfunction

" function! LaTeXUnicodeOn()
"     if &ft ==# "tex"
"         call LaTeXtoUnicode#Enable()
"     elseif &ft ==# "snippets"
"         call LaTeXtoUnicode#Enable()
"     endif
" endfunction

" augroup LatexUnicode
"     " Don't allow autoexpand if a tex or snippet file has been opened in a
"     " buffer, because the g:latex_to_unicode_file_types doesn't work
"     autocmd!
"     autocmd BufEnter * call LaTeXUnicodeOff()
"     autocmd BufLeave * call LaTeXUnicodeOn()
" augroup end

let g:latex_to_unicode_auto=1 " Allow some symbols to be auto-expanded
let g:latex_to_unicode_file_types='.*'
let g:latex_to_unicode_file_types_blacklist = ["tex", "snippets"]
let g:julia_blocks=1
let g:julia_indent_align_brackets=0
let g:julia_indent_align_funcargs=0
let g:julia_highlight_operators=1
let g:julia_spellcheck_comments=1
let g:julia_spellcheck_docstrings=1
let g:latex_to_unicode_tab=1
let g:latex_to_unicode_eager=1

nnoremap <leader>jf :call julia#toggle_function_blockassign()<cr>

"}}}

""" Vimgrep, Grep, Quickfix, and Location list -----------------------------{{{
packadd cfilter
nnoremap [C :colder <cr>
nnoremap ]C :cnewer <cr>
nnoremap [L :lolder <cr>
nnoremap ]L :lnewer <cr>
nnoremap <c-w>lo :lopen <cr>
nnoremap <c-w>lc :lclose <cr>
nnoremap [l :lprevious<cr>
nnoremap ]l :lnext<cr>
nnoremap <c-w>co :copen <cr>
nnoremap <c-w>cc :cclose <cr>
nnoremap [c :cprevious<cr>
nnoremap ]c :cnext<cr>
nnoremap <c-w>nc :pclose <cr>

set switchbuf+=usetab

"}}}

" Tags ---------------------------------------------------------------------{{{
nnoremap <leader>tt :tags<cr>:tag<space>
nnoremap <leader>tls :tags<cr>
nnoremap <leader>to :tags<cr>:ptag<space>
nnoremap <leader>tv <c-w>}

" Traverse tag stack
nnoremap <c-t>c :tabc<cr>
nnoremap <c-t>q :tabc<cr>
nnoremap <leader>tk <c-t>
nnoremap <leader>tj <c-]>
nnoremap <leader>tp <c-t>
nnoremap <leader>tn <c-]>
nnoremap <c-[> <c-t>
nnoremap <c-t> <nop>

" Jump to matching tag
nnoremap <leader>tl :tnext<cr>
nnoremap <leader>th :tprev<cr>

" Open tag in split window
" Horizontal
nmap <leader>t- <c-w>]
" Vertical
nnoremap <c-w><c-[> <c-w>v<c-]>
nnoremap <c-w>[ <c-w>v<c-]>
nnoremap <leader>t\ <c-w>v<c-]>

" Open tag in new tab (similar to <c-w>gf)
nnoremap <c-w>gt mq:tabnew %<cr>`q<c-]>
vnoremap <c-w>gt <esc>mq:tabnew %<cr>gv<c-]>
"}}}

" UltiSnips Options --------------------------------------------------------{{{
let g:UltiSnipsExpandTrigger="<c-i>"
let g:UltiSnipsJumpForwardTrigger="<leader>un" " Next snippet arg
let g:UltiSnipsJumpBackwardTrigger="<leader>up" " Previous snippet arg
let g:ultisnips_python_style="numpy"
let g:UltiSnipsNoPythonWarning=1

"" Only use snippets defined in my version of the vim-snippets repository, this
"" will be for Ulti and snipMate snippets
"let g:UltiSnipsSnippetDirectories=["~/.vim/pack/plugins/start/vim-snippets"]

" Disable snipMate snippets -- if needed, copy to UltiSnips
let g:UltiSnipsEnableSnipMate=0
" }}}

" Inkscape Figures ---------------------------------------------------------{{{
" https://github.com/gillescastel/inkscape-figures
" https://castel.dev/post/lecture-notes-2/
"
" Place this at top of LaTeχ file:
"
"   \usepackage{import}
"   \usepackage{pdfpages}
"   \usepackage{transparent}
"   \usepackage{xcolor}
"
"   \newcommand{\incfig}[2][1]{%
"	\def\svgwidth{#1\columnwidth}
"	\import{./Figures/}{#2.pdf_tex}
"   }
"
"   \pdfsuppresswarningpagegroup=1
" inoremap <C-f> <Esc>: silent exec '.!inkscape-figures create "'.getline('.').'" "'.b:vimtex.root.'/figures/"'<CR><CR>:w<CR>
" nnoremap <C-f> : silent exec '!inkscape-figures edit "'.b:vimtex.root.'/Figures/" > /dev/null 2>&1 &'<CR><CR>:redraw!<CR>
" }}}

" VimTex -------------------------------------------------------------------{{{
let g:tex_flavor='latex'
let g:vimtex_quickfix_mode=0

" Set how citations are concealed
let g:vimtex_syntax_conceal_cites = {
          \ 'type': 'icon',
          \ 'icon': '📖',
          \ 'verbose': v:true,
          \}

" Set PDF viewer
let g:vimtex_view_method = 'zathura'
let g:vimtex_view_automatic = 0

augroup LaTeX
    autocmd!
    autocmd filetype tex nnoremap <localleader>v :VimtexView<cr>
augroup end

let g:vimtex_compiler_method = "latexmk"
let g:vimtex_compiler_latexmk = {
	    \'options': [
		\'--shell-escape'
	    \],
\}
" }}}

" NerdCommenter ------------------------------------------------------------{{{
" " Create default mappings
" let g:NERDCreateDefaultMappings = 1

" " Add spaces after comment delimiters by default
" let g:NERDSpaceDelims = 1

" " Don't use compact syntax for prettified multi-line comments
" let g:NERDCompactSexyComs = 0

" " Align line-wise comment delimiters flush left instead of following code indentation
" let g:NERDDefaultAlign = 'left'

" " Enable NERDCommenterToggle to check all selected lines is commented or not
" let g:NERDToggleCheckAllLines = 1

" " Remove trailing whitespace when uncommenting
" let g:NERDRemoveExtraSpace = 1

" " Set the default comments of Python and Julia to not include a space
" let g:NERDCustomDelimiters = {
"	    \ 'python' : { 'left': '#' },
"	    \ 'julia'  : { 'left': '#' },
"	    \ }
" }}}

" Tmux-Vim-Navigator -------------------------------------------------------{{{
" Disable tmux navigator when zooming the vim pane
let g:tmux_navigator_disable_when_zoomed = 1
let g:tmux_navigator_no_wrap = 1

let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <c-j> :TmuxNavigateDown<cr>
nnoremap <silent> <c-k> :TmuxNavigateUp<cr>
nnoremap <silent> <c-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <c-l> :TmuxNavigateRight<cr>
nnoremap <silent> <c-<space>> :TmuxNavigatePrevious<cr>

nnoremap <silent> <c-w><c-j> <c-w>J
nnoremap <silent> <c-w><c-k> <c-w>K
nnoremap <silent> <c-w><c-h> <c-w>H
nnoremap <silent> <c-w><c-l> <c-w>L

tnoremap <silent> <c-j> <c-w>:TmuxNavigateDown<cr>
tnoremap <silent> <c-k> <c-w>:TmuxNavigateUp<cr>
tnoremap <silent> <c-h> <c-w>:TmuxNavigateLeft<cr>
tnoremap <silent> <c-l> <c-w>:TmuxNavigateRight<cr>
tnoremap <silent> <c-<space>> <c-w>:TmuxNavigatePrevious<cr>

tnoremap <silent> <c-w><c-j> <c-w>J
tnoremap <silent> <c-w><c-k> <c-w>K
tnoremap <silent> <c-w><c-h> <c-w>H
tnoremap <silent> <c-w><c-l> <c-w>L

" }}}

" Python-Mode --------------------------------------------------------------{{{
" Turn on syntax highlighting
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_exit_as_function = 1
let g:pymode_syntax_print_as_function = 1
let g:pymode_syntax_builtin_objs = 1
let g:pymode_syntax_builtin_types = 1
let g:pymode_highlight_execptions = 1
let g:pymode_syntax_docstrings = 1

" Python documentation
let g:pymode_doc = 1
let g:pymode_doc_bind = 'doc'

" Linting
let g:pymode_lint=1
let g:pymode_lint_checkers = ['pep8']
" }}}

" WinResizer ---------------------------------------------------------------{{{
" Use same map to start wince sizer for GUI and terminal
let g:winresizer_start_key = '<c-w><c-a>'
let g:winresizer_gui_start_key = g:winresizer_start_key

" Ensure that WinResizer works in vim terminal mode
tnoremap <c-w><c-a> <c-w>:WinResizerStartResize<cr>

" Disable in GUI since I never use a GUI
let g:winresizer_gui_enable = 0

" Number of points when residing
let g:winresizer_vert_resize=3
let g:winresizer_horiz_resize=3
" }}}

" Spelling -----------------------------------------------------------------{{{
" Set spell check on
set spell spelllang=en_ca

" Prompt fix words
nnoremap <c-s><c-b> [sz=
inoremap <c-s><c-b> <esc>[sz=
nnoremap <c-s><c-f> ]sz=
inoremap <c-s><c-f> <esc>]sz=

" Fix words
nnoremap <c-s>b mq[s1z=`q
inoremap <c-s>b <esc>mq[s1z=`qa
nnoremap <c-s>f mq]s1z=`q
inoremap <c-s>f <esc>mq[s1z=`qa

" Set good word
nnoremap <c-s>gf mq]s1zg`q
inoremap <c-s>gf <esc>mq[szg`qa
nnoremap <c-s>gb mq[s1zg`q
inoremap <c-s>gb <esc>mq[szg`qa

" Set wrong word
nnoremap <c-s>wf mq]s1zw`q
inoremap <c-s>wf <esc>mq[szw`qa
nnoremap <c-s>wb mq[s1zw`q
inoremap <c-s>wb <esc>mq[szw`qa

" Change the spelling highlight groups
if g:colors_name ==? "default"
    highlight clear SpellBad
    highlight SpellBad cterm=underline gui=undercurl
    highlight clear SpellCap
    highlight SpellCap ctermbg=blue ctermfg=white guibg=blue guifg=white
    highlight clear SpellLocal
    highlight SpellLocal ctermbg=magenta ctermfg=white guibg=magenta guifg=white
    highlight clear SpellRare
    highlight SpellRare ctermbg=green ctermfg=white guibg=green guifg=white
endif
" }}}

" Vim-Go -------------------------------------------------------------------{{{
" Using GoVim rather than Vim-Go
" let g:go_highlight_operators = 1
" let g:go_highlight_functions = 1
" let g:go_highlight_function_parameters = 0
" let g:go_highlight_function_calls = 1
" let g:go_highlight_types = 1

" " Change the documentation maps to 'doc', since 'K' is remapped below
" " Also, we need to disable 'K' to open documentation, since plugins are loaded
" " after this file. If we leave this setting enabled, then Vim-Go will overwrite
" " our mapping (below) of 'K' to join the current line with  that above.
" let g:go_doc_keywordprg_enabled = 0
" augroup Documentation
"     autocmd!
"     autocmd FileType go nnoremap doc :GoDoc<cr>
"     autocmd FileType go vnoremap doc :GoDoc<cr>
" augroup end

" " Enable gopls
" let g:go_def_mode = 'gopls'
" let g:go_info_mode = 'gopls'
" }}}

" Tags & Gutentags  --------------------------------------------------------{{{
" And Gutentags+

" Enable gtags modules
let g:gutentags_modules = ['ctags']

" Config project root markers
let g:gutentags_project_root = ['.root']

" Hide tag files
let g:gutentags_ctags_tagfile = ".tags"

" Change focus to quickfix window after search
let g:gutentags_plus_switch = 1

" Output info while running
let g:gutentags_trace = 0

let g:gutentags_generate_on_write = 1

let g:tagbar_type_julia = {
	    \ 'ctagstype' : 'julia',
	    \'kinds' : ['a:abstract', 'i:immutable', 't:type', 'f:function', 'm:macro']
	    \}

let g:gutentags_ctags_exclude_wildignore = 0
let g:gutentags_ctags_exclude = []

" }}}

" Sub Mode --------------------------------------------------------------{{{
let g:submode_always_show_submode = 1
" }}}

" Window Mode --------------------------------------------------------------{{{
" With winresizer, this mode is obselete, since winresizer is better at
" resizing windows than this is! This would be good for creating new windows,
" but then a full mode is overkill
"
" call submode#enter_with('window', 'n', '', '<c-w>')
" call submode#heave_with('window', 'n', '', '<esc>')
" call submode#leave_with('window', 'n', '', 'jk')

" for key in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
"             \'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
"     " Maps lowercase, uppercase, and <c-key>
"     call submode#map('window', 'n', '', key, '<c-w>' . key)
"     call submode#map('window', 'n', '', toupper(key), '<c-w>' . toupper(key))
"     call submode#map('window', 'n', '', '<c-' . key . '>', '<c-w>' . '<c-' .  key . '>')
" endfor

" " Go through symbols. '|' not supported by submode
" for key in ['=', '_', '+', '-', '<', '>']
"     call submode#map('window', 'n', '', key, '<c-w>' . key)
" endfor

" Access window mode the old way using leader
" nnoremap <leader>w <c-w>
" }}}

" Vim-EasyComplete ---------------------------------------------------------{{{
" nnoremap <silent> <leader>nd :EasyCompleteNextDiagnostic<CR>
" nnoremap <silent> <leader>pd :EasyCompletePreviousDiagnostic<CR>
" nnoremap gd :EasyCompleteGotoDefinition<CR>
" nnoremap gr :EasyCompleteReference<CR>

" let g:easycomplete_tab_trigger = ""

" " Enable tabnine AI completion
" let g:easycomplete_tabnine_enable=0
" let g:easycomplete_tabnine_config = {
"	    \ 'line_limit': 1000,
"	    \ 'max_num_result': 3,
"	    \ }

" let g:easycomplete_menu_skin = {
"     \   "buf": {
"     \      "kind":"⚯",
"       \      "menu":"[B]",
"       \    },
"       \   "snip": {
"     \      "kind":"<>",
"       \      "menu":"[S]",
"       \    },
"       \   "dict": {
"     \      "kind":"d",
"       \      "menu":"[🕮]",
"       \    },
"       \   "tabnine": {
"     \      "kind":"",
"     \      "menu": "[TN]",
"       \    },
"       \ }

" let g:easycomplete_lsp_type_font = {
"     \ 'text' : '',         'method':'m',    'function': 'f',
"       \ 'constructor' : '≡',  'field': 'f',    'default':'d',
"       \ 'variable' : '𝘤',     'class':'c',     'interface': 'i',
"       \ 'module' : 'm',       'property': 'p', 'unit':'u',
"       \ 'value' : '𝘧',        'enum': 'e',     'keyword': 'k',
"       \ 'snippet': '𝘧',       'color': 'c',    'file':'',
"       \ 'reference': 'r',     'folder': '',   'enummember': 'e',
"       \ 'constant':'c',       'struct': 's',   'event':'e',
"       \ 'typeparameter': 't', 'var': 'v',      'const': 'c',
"       \ 'operator':'o',
"       \ 't':'𝘵',   'f':'𝘧',   'c':'𝘤',   'm':'𝘮',   'u':'𝘶',   'e':'𝘦',
"       \ 's':'𝘴',   'v':'𝘷',   'i':'𝘪',   'p':'𝘱',   'k':'𝘬',   'r':'𝘳',
"       \ 'o':"𝘰",   'l':"𝘭",   'a':"𝘢",   'd':'𝘥',
"       \ }

" let g:easycomplete_sign_text = {
"       \   'error':       "!",
"       \   'warning':     "▲",
"       \   'information': 'i',
"       \   'hint':        '▧'
"       \ }

" " }}}

" Auto Popup ---------------------------------------------------------------{{{
let g:apc_enable_ft = {'julia': 1}
" }}}

" Netrw --------------------------------------------------------------------{{{
" https://vonheikemen.github.io/devlog/tools/using-netrw-vim-builtin-file-explorer/
" https://gist.github.com/t-mart/610795fcf7998559ea80
" Look at adjusting this section with netrw#UserMaps
let g:netrw_banner = 0
let g:netrw_liststyle = 0
" let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_alto = 1
let g:netrw_winsize = 17
let g:netrw_keepdir = 0
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'
let g:netrw_localcopydircmd = 'cp -r'
let g:netrw_special_syntax = 1
let g:netrw_fastbrowse = 2
let g:netrw_browse_split = 4

nnoremap <leader>d :Lexplore<cr>

" NetrwMapping
"   Set mappings for netrw
"
"   Navigation
"	  H:	Go back up in history
"	  h:	Go up a directory
"	  l:	Open a directory or file
"	  .:	Toggle the dotfiles
"	  P:	Close the preview window
"	  L:	Open a file and close netrw
" <leader>d:	Close/open netrw
"	 ^r:	Refresh
"
"   File Managing
"	t:	Open file in new tab
"	f:	Create a file
"	d:	Create a directory
"	R:	Rename a file
"	z:	List marked files
"	T:	Show target directory
"	x/^x:	Open with default application
"	D:	Delete a file or an empty directory
"      RD:	Recursively delete a directory
"
"   Bookmarks
"	bb:	Create bookmark in current directory
"	bd:	Delete most recent bookmark
"	bj:	Jump to most recent bookmark
function! NetrwMapping()
    " Navigation
    nmap <buffer> <c-r> :e .<cr>
    nmap <buffer> H u
    nmap <buffer> h -
    nmap <buffer> l <cr>
    nmap <buffer> . gh
    nmap <buffer> P <c-w>z
    nmap <buffer> <leader>d :Lexplore<cr>
    nmap <buffer> L <cr><c-w><c-p>:q<cr>

    " nmap <buffer> v v<c-w>q

    " Keep similar navigation for pane switching
    nmap <buffer> <c-l> <c-w>l
    nmap <buffer> <c-h> <c-w>h
    nmap <buffer> <c-k> <c-w>k
    nmap <buffer> <c-j> <c-w>j
    nmap <buffer> <leader>nhs :nohlsearch<cr>

    " File managing
    nmap <buffer> f %:w<CR>:buffer #<CR>
    nmap <buffer> z :echo join(netrw#Expose("netrwmarkfilelist"), "\n")<CR>
    nmap <buffer> T :echo 'Target:' . netrw#Expose("netrwmftgt")<CR>
    nmap <buffer> <c-x> gxA

    " Recursively delete directories
    nmap <buffer> RD :call NetrwRemoveRecursive()<cr>

    " Bookmark
    nmap <buffer> bb mb
    nmap <buffer> bd mB
    nmap <buffer> bj gbs

endfunction

function! NetrwRemoveRecursive()
    if &filetype ==# 'netrw'
	cnoremap <buffer> <CR> rm -r<CR>
	normal mu
	normal mf

    try
	normal mx
    catch
	echo "Cancelled"
    endtry

    cunmap <buffer> <CR>
    endif
endfunction

augroup netrw_mapping
    autocmd!
    autocmd filetype netrw call NetrwMapping()
augroup end
" }}}

" Vim-EasyMotion -----------------------------------------------------------{{{
" Easy motion is triggerd with <leader>e
map <leader><space> <plug>(easymotion-prefix)
map <leader><space>/ <plug>(easymotion-sn)

let g:EasyMotion_cursor_highlight=1
let g:EasyMotion_move_highlight=1
let g:EasyMotion_inc_highlight=1
let g:EasyMotion_landing_highlight=0
let g:EasyMotion_do_mapping=1

let g:EasyMotion_off_screen_search=1

let g:EasyMotion_enter_jump_first=1
let g:EasyMotion_space_jump_first=1

" map s <plug>(easymotion-s)
" noremap <leader>s s

" map f <plug>(easymotion-fl)
" map F <plug>(easymotion-Fl)
" map t <plug>(easymotion-tl)
" map T <plug>(easymotion-Tl)

" noremap <leader>f f
" noremap <leader>F F
" noremap <leader>t t
" noremap <leader>T T

"}}}

" RainbowCSV ---------------------------------------------------------------{{{
let g:rcsv_colorlinks = ['RainbowCSV0', 'RainbowCSV1', 'RainbowCSV2', 'RainbowCSV3', 'RainbowCSV4', 'RainbowCSV5', 'RainbowCSV6', 'RainbowCSV7', 'RainbowCSV8', 'RainbowCSV9', 'RainbowCSV10', 'RainbowCSV11', 'RainbowCSV12', 'RainbowCSV13']

" }}}

" Unicode.Vim --------------------------------------------------------------{{{
let g:Unicode_ShowPreviewWindow = 1
" }}}

" Papis --------------------------------------------------------------------{{{
function! PapisBibtexRef()
  let l:temp = tempname()
  echom l:temp
  silent exec "!papis bibtex ref -o ".l:temp
  let l:olda = @a
  let @a = join(readfile(l:temp), ',')
  normal! "ap
  redraw!
  let @a = l:olda
endfunction

command! -nargs=0 BibRef call PapisBibtexRef()
command! -nargs=0 BibOpen exec "!papis bibtex open"

" }}}

" Vim-Yoink ----------------------------------------------------------------{{{
nmap p <plug>(YoinkPaste_p)
nmap P <plug>(YoinkPaste_P)

nnoremap <leader>yls :Yanks<cr>
nnoremap <leader>yc :ClearYanks<cr>

let g:yoinkSyncNumberedRegisters = 1 " Yank to numbered registers
let g:yoinkIncludeDeleteOperations = 1

" Also replace the default gp with yoink paste so we can toggle paste in this case too
nmap gp <plug>(YoinkPaste_gp)
nmap gP <plug>(YoinkPaste_gP)

nmap <c-y><c-p> <plug>(YoinkPostPasteSwapBack)
nmap <c-y><c-n> <plug>(YoinkPostPasteSwapForward)
nmap [y <plug>(YoinkPostPasteSwapBack)
nmap ]y <plug>(YoinkPostPasteSwapForward)
nmap [Y <plug>(YoinkRotateBack)
nmap ]Y <plug>(YoinkRotateForward)

nmap <c-=> <plug>(YoinkPostPasteToggleFormat)
" }}}

" Vim-Subversive -----------------------------------------------------------{{{
" s for substitute
nmap s <plug>(SubversiveSubstitute)
vmap s <plug>(SubversiveSubstitute)
nmap ss <plug>(SubversiveSubstituteLine)
nmap S <plug>(SubversiveSubstituteToEndOfLine)

nmap <leader>s <plug>(SubversiveSubstituteRange)
xmap <leader>s <plug>(SubversiveSubstituteRange)

nmap <leader>ss <plug>(SubversiveSubstituteWordRange)
" }}}

" Vim-Cutlass --------------------------------------------------------------{{{
nnoremap m d
xnoremap m d

" Don't clobber yanks with paste
vnoremap p "_dP

nnoremap mm dd
nnoremap M D

" Remap mark keys
nnoremap gm `
nnoremap gmm ``
nnoremap sm m
" nnoremap <leader>ms m
" nnoremap <leader>mg `
" nnoremap <leader>ml '
" }}}

" Choosewin ----------------------------------------------------------------{{{
nmap  <c-w>w  <Plug>(choosewin)
nmap  <c-w><c-w>  <Plug>(choosewin)
tmap  <c-w>w  <c-w>N<Plug>(choosewin)
tmap  <c-w><c-w>  <c-w>N<Plug>(choosewin)

" use overlay feature
let g:choosewin_overlay_enable = 0

" workaround for the overlay font being broken on mutibyte buffer.
let g:choosewin_overlay_clear_multibyte = 1

" tmux-like overlay color
let g:choosewin_color_overlay = {
      \ 'cterm': [8, 8]
      \ }
let g:choosewin_color_overlay_current = {
      \ 'cterm': [4, 4]
      \ }

let g:choosewin_blink_on_land      = 0
let g:choosewin_statusline_replace = 1
let g:choosewin_tabline_replace    = 1

let g:choosewin_color_label_current = { 'cterm': [ 4, 0, 'bold,italic'] }
let g:choosewin_color_label = { 'cterm': [ 8, 15, 'bold,italic'] }
let g:choosewin_color_other = { 'cterm': [ 8, 8, 'bold,italic'] }
let g:choosewin_color_land = { 'cterm': [ 4, 0, 'bold,italic'] }
let g:choosewin_color_shade = { 'cterm': [ 0, 0, ''] }
" }}}

" Obsession --------------------------------------------------------------- {{{
let g:obsession_no_bufenter = 1
command! Obsess :Obsession .session.vim
" }}}

" Twiggy --------------------------------------------------------------- {{{
command! T :Twiggy

" Group locals branches by most recently used, rather than by slash
let g:twiggy_group_locals_by_slash = 0
let g:twiggy_local_branch_sort = 'mru'

" Sort remote branches by date
let g:twiggy_remote_branch_sort = 'date'

let g:twiggy_set_upstream=0

nnoremap <leader>G :tab G \| T<cr>
" }}}

" Command-line maps -------------------------------------------------------{{{
cnoremap <c-j> <down>
cnoremap <c-k> <up>
"}}}

" Yanking, selecting, cutting ----------------------------------------------{{{
nnoremap Y yg_

if s:linux
    " <alt-v> to select full line without trailing newline
    nnoremap v _vg_
    nnoremap y yg_
    nnoremap Y mqy^`q
    nmap m mg_
    nmap M m^
elseif s:darwin
    nnoremap √ _vg_
endif
" }}}

" Remove arrow keys, esc, backspace ------------{{{
inoremap <esc> <nop>
vnoremap <esc> <nop>
noremap <Down> <nop>
noremap <Up> <nop>
noremap <left> <nop>
noremap <right> <nop>
noremap <Down> <nop>

" inoremap <BS> <nop>
" cnoremap <BS> <nop>
"}}}

" Terminal opening and navigation ------------------------------------------{{{
nnoremap <c-w>t\ :vert term ++close<cr>
nnoremap <c-w>t- :term ++close<cr>
nnoremap <c-t>t :tab term ++close<cr>
nnoremap <c-t><c-t> :tab term ++close<cr>
tnoremap <c-w>t\ <c-w>:term ++close<cr>
tnoremap <c-w>t- <c-w>:term ++close<cr>
tnoremap <c-t>t <c-w>:tab term ++close<cr>

tnoremap <c-w><c-n> <c-w>N
" tnoremap <c-w>N <c-w>N<cr>

nnoremap <c-w><c-s> :shell<cr>
nnoremap <c-w>s :shell<cr>

" Quick scrolling
tnoremap <c-w><c-b> <c-w>N<c-b>
tnoremap <c-w><c-u> <c-w>N<c-u>
tnoremap <c-w><c-f> <c-w>N<c-f>
tnoremap <c-w><c-d> <c-w>N<c-d>
"}}}

" Tab navigation -----------------------------------------------------------{{{
nnoremap <c-t>` :tabfirst<cr>
nnoremap <c-t>1 1gt
nnoremap <c-t>2 2gt
nnoremap <c-t>3 3gt
nnoremap <c-t>4 4gt
nnoremap <c-t>5 5gt
nnoremap <c-t>6 6gt
nnoremap <c-t>7 7gt
nnoremap <c-t>8 8gt
nnoremap <c-t>9 9gt
nnoremap <c-t>0 10gt
nnoremap <c-t>- :tablast<cr>

tnoremap <c-t>` <c-w>:tabfirst<cr>
tnoremap <c-t>1 <c-w>1gt
tnoremap <c-t>2 <c-w>2gt
tnoremap <c-t>3 <c-w>3gt
tnoremap <c-t>4 <c-w>4gt
tnoremap <c-t>5 <c-w>5gt
tnoremap <c-t>6 <c-w>6gt
tnoremap <c-t>7 <c-w>7gt
tnoremap <c-t>8 <c-w>8gt
tnoremap <c-t>9 <c-w>9gt
tnoremap <c-t>0 <c-w>10gt
tnoremap <c-t>- <c-w>:tablast<cr>

nnoremap <c-t>ob :buffers<cr>:tab :sb<space>

" Switch tabs
" nnoremap <silent><c-p> :<c-u>execute "silent! normal! " . ((tabpagenr() + v:count1 - 1) % tabpagenr("$") + 1) . "gt"<cr>
nnoremap <silent>]t :<c-u>execute "silent! normal! " . ((tabpagenr() + v:count1 - 1) % tabpagenr("$") + 1) . "gt"<cr>
" nnoremap <silent><c-t>l :<c-u>execute "silent! normal! " . ((tabpagenr() + v:count1 - 1) % tabpagenr("$") + 1) . "gt"<cr>
nnoremap <silent><c-t>n :<c-u>execute "silent! normal! " . ((tabpagenr() + v:count1 - 1) % tabpagenr("$") + 1) . "gt"<cr>
nnoremap <silent><c-t><c-n> :<c-u>execute "silent! normal! " . ((tabpagenr() + v:count1 - 1) % tabpagenr("$") + 1) . "gt"<cr>
tnoremap <silent>]t <c-w>gt
tnoremap <silent><c-t><c-n> <c-w>gt
tnoremap <silent><c-t>n <c-w>gt

nnoremap <silent>[t gT
nnoremap <silent><c-t>p gT
nnoremap <silent><c-t><c-p> gT

tnoremap <silent>[t <c-w>gT
tnoremap <silent><c-t>p <c-w>gT
tnoremap <silent><c-t><c-p> <c-w>gT

tnoremap <silent>1[t <c-w>1gT
tnoremap <silent>1<c-t>p <c-w>1gT
tnoremap <silent>1<c-t><c-p> <c-w>1gT

tnoremap <silent>2[t <c-w>2gT
tnoremap <silent>2<c-t>p <c-w>2gT
tnoremap <silent>2<c-t><c-p> <c-w>2gT

tnoremap <silent>3[t <c-w>3gT
tnoremap <silent>3<c-t>p <c-w>3gt
tnoremap <silent>3<c-t><c-p> <c-w>3gt

tnoremap <silent>4[t <c-w>4gT
tnoremap <silent>4<c-t>p <c-w>4gt
tnoremap <silent>4<c-t><c-p> <c-w>4gt

tnoremap <silent>5[t <c-w>5gT
tnoremap <silent>5<c-t>p <c-w>5gt
tnoremap <silent>5<c-t><c-p> <c-w>5gt

" Write tab
nnoremap <c-t>w :w<cr>
nnoremap <c-t>wq :wq<cr>

" New tab
nnoremap <c-t>b :tabnew<cr>
tnoremap <c-t>b <c-w>:tabnew<cr>

" Edit in new tab
nnoremap <c-t>e :tabedit<space>
tnoremap <c-t>e <c-w>:tabedit<space>

" List tabs
nnoremap <c-t>ls :tabs<cr>

" Move tabs
nnoremap ]T :<c-u>execute "silent! normal! :tabm +" . v:count1 . "\r"<cr>
nnoremap <c-t>N :<c-u>execute "silent! normal! :tabm +" . v:count1 . "\r"<cr>
tnoremap ]T <c-w>:tabm +1<cr>
tnoremap <c-t>N <c-w>:tabm +1<cr>

nnoremap [T :<c-u>execute "silent! normal! :tabm -" . v:count1 . "\r"<cr>
nnoremap <c-t>P :<c-u>execute "silent! normal! :tabm -" . v:count1 . "\r"<cr>
tnoremap [T <c-w>:tabm -1<cr>
tnoremap <c-t>P <c-w>:tabm -1<cr>

" if s:os ==? "Darwin"
"     nnoremap ø :<c-u>execute "silent! normal! :tabm -" . v:count1 . "\r"<cr>
"     nnoremap π :<c-u>execute "silent! normal! :tabm +" . v:count1 . "\r"<cr>
" elseif s:os ==? "Linux"
"     " See :help map-alt-keys
"     " https://vi.stackexchange.com/questions/2350/how-to-map-alt-key
"     " http://www.leonerd.org.uk/hacks/fixterms/
"     "
"     " The following maps will work if your terminal emulator sends a key with
"     " the 8th bit set when modifying the key with <Alt>. Most terminal
"     " emulators do not do this by default, but instead send an <Esc> before the
"     " key when modifying it with <Alt>. URxvt and xterm both support both
"     " behaviours of <Alt>, but the <Esc> prefix is default.
"     " nnoremap <m-o> :tabm -1<cr>
"     " nnoremap <m-p> :tabm +1<cr>

"     " The following lines should work
"     " for most terminal emulators that prefix a key with <Esc> when modifying
"     " that key with <Alt>.
"     nnoremap o :<c-u>execute "silent! normal! :tabm -" . v:count1 . "\r"<cr>
"     nnoremap p :<c-u>execute "silent! normal! :tabm +" . v:count1 . "\r"<cr>
" endif
" }}}

" Window/Pane navigation ---------------------------------------------------{{{
" Pane navigation
" Use hotkeys similar to vim-tmux-navigator if the plugin is not used.
" nnoremap <c-h> <c-w>h
" nnoremap <c-j> <c-w>j
" nnoremap <c-k> <c-w>k
" nnoremap <c-l> <c-w>l

" Additionally, add some keybindings so that we can easily jump in and out of
" terminal windows. We need to preface with the leader key so that terminal
" applications like fzf and papis continue working with the <c-j> and <c-k>
" mappings. Alternatively, you can just use <c-w><movement key>, which happens
" to have the same number of keystrokes.
" tnoremap <leader><c-h> <c-w>h
" tnoremap <leader><c-j> <c-w>j
" tnoremap <leader><c-k> <c-w>k
" tnoremap <leader><c-l> <c-w>l

" Pane swapping and resizing
" Taken care of by WinResizer plugin

" Pane splitting - similar to Tmux
" - for horizontal split
" \ for vertical split
tnoremap <c-w>- <c-w>:split<cr><c-w>j
tnoremap <c-w>e- <c-w>:split<space>
nnoremap <c-w>- :split<cr><c-w>j
nnoremap <c-w>e- :split<space>
tnoremap <c-w>\ <c-w>:vsplit<cr><c-w>l
tnoremap <c-w>e\ <c-w>:vsplit<space>
nnoremap <c-w>\ :vsplit<cr><c-w>l
nnoremap <c-w>e\ :vsplit<space>

" Hide default window creation maps
nnoremap <c-w><c-n> <nop>
nnoremap <c-w>n <nop>

" Open current window in new tab, while closing it in this tab using map
" similar to tmux
nnoremap <c-w>! :execute "normal! :tabnew %\r:tabp\r:wincmd q\r:tabnext\r"<cr>

" Resize editor window to textwidth
augroup Resize
    autocmd!
    autocmd BufAdd * let editorwidth = &textwidth + 5
    autocmd BufAdd * let termwidth = &columns - (&textwidth + 6)
    autocmd BufAdd * nnoremap <c-w>u :execute "normal! :vertical resize " . editorwidth . "\r"<cr>
    autocmd BufAdd * nnoremap <c-w><c-u> :execute "normal! :vertical resize " . editorwidth . "\r"<cr>
    autocmd BufAdd * tnoremap <c-w>u <c-w>:execute "normal! :vertical resize " . termwidth . "\r"<cr>
    autocmd BufAdd * tnoremap <c-w><c-u> <c-w>:execute "normal! :vertical resize " . termwidth . "\r"<cr>

    autocmd WinEnter *.* let editorwidth = &textwidth + 5
    autocmd WinEnter *.* let termwidth = &columns - (&textwidth + 6)
augroup end
"}}}

" Buffer and arg navigation ------------------------------------------------{{{
" Switching buffers
tnoremap ]b <c-w>:bnext<cr>
tnoremap <c-g>n <c-w>:bnext<cr>
tnoremap <c-g><c-n> <c-w>:bnext<cr>
nnoremap ]b :<c-u>execute "normal! :bnext" . v:count1 . "\r"<cr>
nnoremap <c-g>n :<c-u>execute "normal! :bnext" . v:count1 . "\r"<cr>
nnoremap <c-g><c-n> :<c-u>execute "normal! :bnext" . v:count1 . "\r"<cr>
tnoremap [b <c-w>:bprev<cr>
tnoremap <c-g>p <c-w>:bprev<cr>
tnoremap <c-g><c-p> <c-w>:bprev<cr>
nnoremap [b :<c-u>execute "normal! :bprev" . v:count1 . "\r"<cr>
nnoremap <c-g>p :<c-u>execute "normal! :bprev" . v:count1 . "\r"<cr>
nnoremap <c-g><c-p> :<c-u>execute "normal! :bprev" . v:count1 . "\r"<cr>

nnoremap [a :<c-u>execute "normal! :" . v:count1 . "prev \r"<cr>
nnoremap ]a :<c-u>execute "normal! :" . v:count1 . "next \r"<cr>
tnoremap [a <c-w>:<c-u>execute "normal! :" . v:count1 . "prev \r"<cr>
tnoremap ]a <c-w>:<c-u>execute "normal! :" . v:count1 . "next \r"<cr>

" Open buffers in a new tab
" nnoremap <c-h>t :buffers<cr>:tab sb<space>
nnoremap <c-g>t :<c-u>execute "normal! :tab sb " . v:count1 . "\r"<cr>
tnoremap <c-g>t <c-w>:buffers<cr>:tab sb<space>

" Switch buffer
nnoremap <c-g>s :buffers<cr>:buffer<space>
tnoremap <c-g>s <c-w>:buffers<cr>:buffer<space>

" Recent Buffer - go to recent buffer
nnoremap <c-g><c-r> :e #<cr>
nnoremap <c-g>r :e #<cr>
tnoremap <c-g>r <c-w>:e #<cr>
tnoremap <c-g><c-r> <c-w>:e #<cr>

" List buffers
tnoremap <c-g>ls <c-w>:ls<cr>
nnoremap <c-g>ls :ls<cr>

" New buffer
nnoremap <c-g>b :enew<cr>
tnoremap <c-g>b <c-w>:enew<cr>

" Edit buffer
nnoremap <c-g>e :e<space>
tnoremap <c-g>e <c-w>:e<space>

" Delete buffer
nnoremap <c-g>d! :buffers<cr>:bd!<space>    " Force delete current
nnoremap <c-g>dc :buffers<cr>:bd<space>|    " Choose delete
nnoremap <c-g>dd :bdelete<cr>|		    " Delete current
nnoremap <c-g>do :%bd<cr><c-o>:bd#<cr>|	    " Delete other

tnoremap <c-g>d! <c-w>:buffers<cr>:bd!<space>
tnoremap <c-g>dc <c-w>:buffers<cr>:bd<space>
tnoremap <c-g>dd <c-w>:%bdelete<cr>
tnoremap <c-g>do <c-w>:%bd<cr><c-o>:bd#<cr>

" Write buffer
nnoremap <c-g>w :bufdo w<cr>
nnoremap <c-g>wq :bufdo wq<cr>
"}}}

" Line navigation ----------------------------------------------------------{{{
nnoremap ga g_i
nnoremap gi _a
nnoremap gl g_
onoremap gl g_
nnoremap gh ^
onoremap gh ^

" Move lines
nnoremap [e ddkP
nnoremap ]e ddp

" Create empty lines
nnoremap [o O<esc>
nnoremap ]o o<esc>

nnoremap [p O<esc>p
nnoremap ]p o<esc>p

" Go to textwidth column
onoremap <leader>\| :execute "normal! 0" . &textwidth . "l"<cr>
nnoremap <leader>\| :execute "normal! 0" . &textwidth . "l"<cr>
vnoremap <leader>\| :<c-u>call ToLastCol()<cr>

function ToLastCol()
    execute "normal! gv_" . &textwidth . "l"
endfunction

vnoremap ga g_i
vnoremap gi _a
vnoremap gl g_
vnoremap gh _

" Join and move lines
nnoremap doc K
vnoremap K <nop>
nnoremap K kJ
"}}}

" Remap the <esc> key ------------------------------------------------------{{{
inoremap jk <esc>
vnoremap jk <esc>
tnoremap jk <esc>

" Fix paste bug trigged by the above maps
set t_BE=
" }}}

" Edit and source vimrc ----------------------------------------------------{{{
nnoremap <leader>ev :split $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>
" }}}

" Select inside and around symbols -----------------------------------------{{{

nnoremap val) :<c-u>normal! f)va)<cr>
onoremap al) :<c-u>normal! f)va)<cr>
nnoremap val( :<c-u>normal! f(va(<cr>
onoremap al( :<c-u>normal! f(va(<cr>
nnoremap vah( :<c-u>normal! F(va(<cr>
onoremap ah( :<c-u>normal! F(va(<cr>
nnoremap vah) :<c-u>normal! F)va)<cr>
onoremap ah) :<c-u>normal! F)va)<cr>

nnoremap val] :<c-u>normal! f]va]<cr>
onoremap al] :<c-u>normal! f]va]<cr>
nnoremap val[ :<c-u>normal! f[va[<cr>
onoremap al[ :<c-u>normal! f[va[<cr>
nnoremap vah[ :<c-u>normal! F[va[<cr>
onoremap ah[ :<c-u>normal! F[va[<cr>
nnoremap vah] :<c-u>normal! F]va]<cr>
onoremap ah] :<c-u>normal! F]va]<cr>

nnoremap val} :<c-u>normal! f}va}<cr>
onoremap al} :<c-u>normal! f}va}<cr>
nnoremap val{ :<c-u>normal! f{va{<cr>
onoremap al{ :<c-u>normal! f{va{<cr>
nnoremap vah{ :<c-u>normal! F{va{<cr>
onoremap ah{ :<c-u>normal! F{va{<cr>
nnoremap vah} :<c-u>normal! F}va}<cr>
onoremap ah} :<c-u>normal! F}va}<cr>

nnoremap val> :<c-u>normal! f>va><cr>
onoremap al> :<c-u>normal! f>va><cr>
nnoremap val< :<c-u>normal! f<va<<cr>
onoremap al< :<c-u>normal! f<va<<cr>
nnoremap vah< :<c-u>normal! F<va<<cr>
onoremap ah< :<c-u>normal! F<va<<cr>
nnoremap vah> :<c-u>normal! F>va><cr>
onoremap ah> :<c-u>normal! F>va><cr>

nnoremap val" :<c-u>normal! f"va"<cr>
onoremap al" :<c-u>normal! f"va"<cr>
nnoremap vah" :<c-u>normal! F"va"<cr>
onoremap ah" :<c-u>normal! F"va"<cr>

nnoremap val' :<c-u>normal! f'va'<cr>
onoremap al' :<c-u>normal! f'va'<cr>
nnoremap vah' :<c-u>normal! F'va'<cr>
onoremap ah' :<c-u>normal! F'va'<cr>

nnoremap val$ :<c-u>normal! f$lvf$h<cr>
onoremap al$ :<c-u>normal! f$lvf$h<cr>
nnoremap vah$ :<c-u>normal! F$hvF$l<cr>
onoremap ah$ :<c-u>normal! F$hvF$l<cr>

nnoremap val` :<c-u>normal! f`va`<cr>
onoremap al` :<c-u>normal! f`va`<cr>
nnoremap vah` :<c-u>normal! F`va`<cr>
onoremap ah` :<c-u>normal! F`va`<cr>

nnoremap vil) :<c-u>normal! f)vi)<cr>
onoremap il) :<c-u>normal! f)vi)<cr>
nnoremap vil( :<c-u>normal! f(vi(<cr>
onoremap il( :<c-u>normal! f(vi(<cr>
nnoremap vih( :<c-u>normal! F(vi(<cr>
onoremap ih( :<c-u>normal! F(vi(<cr>
nnoremap vih) :<c-u>normal! F)vi)<cr>
onoremap ih) :<c-u>normal! F)vi)<cr>

nnoremap vil] :<c-u>normal! f]vi]<cr>
onoremap il] :<c-u>normal! f]vi]<cr>
nnoremap vil[ :<c-u>normal! f[vi[<cr>
onoremap il[ :<c-u>normal! f[vi[<cr>
nnoremap vih[ :<c-u>normal! F[vi[<cr>
onoremap ih[ :<c-u>normal! F[vi[<cr>
nnoremap vih] :<c-u>normal! F]vi]<cr>
onoremap ih] :<c-u>normal! F]vi]<cr>

nnoremap vil} :<c-u>normal! f}vi}<cr>
onoremap il} :<c-u>normal! f}vi}<cr>
nnoremap vil{ :<c-u>normal! f{vi{<cr>
onoremap il{ :<c-u>normal! f{vi{<cr>
nnoremap vih{ :<c-u>normal! F{vi{<cr>
onoremap ih{ :<c-u>normal! F{vi{<cr>
nnoremap vih} :<c-u>normal! F}vi}<cr>
onoremap ih} :<c-u>normal! F}vi}<cr>

nnoremap vil> :<c-u>normal! f>vi><cr>
onoremap il> :<c-u>normal! f>vi><cr>
nnoremap vil< :<c-u>normal! f<vi<<cr>
onoremap il< :<c-u>normal! f<vi<<cr>
nnoremap vih< :<c-u>normal! F<vi<<cr>
onoremap ih< :<c-u>normal! F<vi<<cr>
nnoremap vih> :<c-u>normal! F>vi><cr>
onoremap ih> :<c-u>normal! F>vi><cr>

nnoremap vil" :<c-u>normal! f"vi"<cr>
onoremap il" :<c-u>normal! f"vi"<cr>
nnoremap vih" :<c-u>normal! F"vi"<cr>
onoremap ih" :<c-u>normal! F"vi"<cr>

nnoremap vil' :<c-u>normal! f'vi'<cr>
onoremap il' :<c-u>normal! f'vi'<cr>
nnoremap vih' :<c-u>normal! F'vi'<cr>
onoremap ih' :<c-u>normal! F'vi'<cr>

nnoremap vil$ :<c-u>normal! f$lvf$h<cr>
onoremap il$ :<c-u>normal! f$lvf$h<cr>
nnoremap vih$ :<c-u>normal! F$hvF$l<cr>
onoremap ih$ :<c-u>normal! F$hvF$l<cr>

nnoremap vil` :<c-u>normal! f`vi`<cr>
onoremap il` :<c-u>normal! f`vi`<cr>
nnoremap vih` :<c-u>normal! F`vi`<cr>
onoremap ih` :<c-u>normal! F`vi`<cr>
"}}}

" Vim-Slime ---------------------------------------------------------------{{{
let g:slime_target = "vimterminal"
let g:slime_no_mappings = 1

" nnoremap <leader>sa :packadd vim-slime && packadd vim-slime-cells <cr>
nnoremap <leader>la :execute "normal! :packadd vim-slime\r:packadd vim-slime-cells\r"<cr>
xmap <leader>lr <plug>SlimeRegionSend
nmap <leader>lr <plug>SlimeParagraphSend
nmap <leader>lv <plug>SlimeConfig
nmap <leader>ll <plug>SlimeLineSend
nmap <leader>lm <plug>SlimeMotionSend
nmap <leader>l1 :SlimeSend1<space>

" vim-slime-cells
let g:slime_cells_no_highlight = 0
let g:slime_cells_highlight_from = ""
let g:slime_cells_fg_cterm = "12"

" Set the slime cell delimiter, which is \lceil and \rfloor
let g:slime_cell_delimiter = "^\\s*⌈.*⌋"
iabbrev _newcell ⌈  ⌋
nmap <leader>lc <Plug>SlimeCellsSendAndGoToNext
nmap <leader>ln <Plug>SlimeCellsNext
nmap <leader>lp <Plug>SlimeCellsPrev
nmap ]x <Plug>SlimeCellsNext
nmap [x <Plug>SlimeCellsPrev
"}}}

" Git-Gutter --------------------------------------------------------------{{{
" let g:gitgutter_map_keys = 0

" nmap ]h <Plug>(GitGutterNextHunk)
" nmap [h <Plug>(GitGutterPrevHunk)

" nmap <leader>hp <Plug>(GitGutterPreviewHunk)
" nmap <leader>hs <Plug>(GitGutterStageHunk)
" nmap <leader>hu <Plug>(GitGutterUndoHunk)

" omap ih <Plug>(GitGutterTextObjectInnerPending)
" omap ah <Plug>(GitGutterTextObjectOuterPending)
" xmap ih <Plug>(GitGutterTextObjectInnerVisual)
" xmap ah <Plug>(GitGutterTextObjectOuterVisual)

" command! Gqf GitGutterQuickFix | copen
"}}}

" Vim-Signify --------------------------------------------------------------{{{
let g:signify_sign_change = "~"
let g:signify_sign_delete = "×"
let g:signify_sign_delete_first_line = "×"

" autocmd User SignifyAutocmds exe 'au! signify' | au signify BufWritePost * call sy#start()
autocmd User SignifyAutocmds autocmd! signify CursorHold,CursorHoldI

autocmd InsertLeave * :execute "silent! normal :SignifyRefresh\r"
autocmd InsertEnter * :execute "silent! normal :SignifyRefresh\r"
autocmd InsertChange * :execute "silent! normal :SignifyRefresh\r"


nnoremap \gg :SignifyToggle<cr>
nnoremap \gh :SignifyToggleHighlight<cr>
nnoremap <leader>gr :SignifyRefresh<cr>

nmap ]h <Plug>(signify-next-hunk)
nmap [h <Plug>(signify-prev-hunk)

omap ih <plug>(signify-motion-inner-pending)
xmap ih <plug>(signify-motion-inner-visual)
omap ah <plug>(signify-motion-outer-pending)
xmap ah <plug>(signify-motion-outer-visual)
" }}}

" Vimwiki ------------------------------------------------------------------{{{
let g:vimwiki_list = [{"path": ".vimwiki"}]
"}}}

" QuickUI -----------------------------------------------------------------{{{

let opts = {'w':60, 'h':8, 'border': 1}
let opts.title = 'Terminal Popup'
nnoremap <silent> <c-q>jl :execute "call quickui#terminal#open('julia', opts)"<cr>
nnoremap <silent> <c-q>py :execute "call quickui#terminal#open('python', opts)"<cr>
nmap <silent> <c-q>hl :execute "call quickui#tools#display_help('" . expand('<cword>') . "') "<cr>

command -nargs=1 Qhelp :call quickui#tools#display_help(<f-args>)
command -nargs=1 Qh :call quickui#tools#display_help(<f-args>)
command Qjulia :call quickui#terminal#open('julia', opts)
command Qjl :Qjulia
command Qpython :call quickui#terminal#open('ipython', opts)
command Qpy Qpython
command Qzsh :call quickui#terminal#open('zsh', opts)
command Qz Qzsh
command Qbash :call quickui#terminal#open('bash', opts)
command Qb Qbash

let g:quickui_border_style = 2
let g:quickui_color_scheme = 'base16'
"}}}

"}}}

" Use the 16 ansi colours from the terminal in terminal buffers instead of the
" default 256 colours
" For some reason this isn't working...
" let &t_Co=16
" set t_Co=16
" set t_RV=

nnoremap <esc> <nop>
nnoremap <c-w>n <nop>
nnoremap <c-w><c-n> <nop>
